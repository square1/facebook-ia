<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class TwitterEmbedsTest extends TestCase
{

    /**
     * Test the twitter embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatTwitter()
    {
        $htmlContent = '<p>Enjoy/wince...</p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><p>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatTwitter();

        $expected = '<p>Enjoy/wince...</p><figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure><a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a>
            <figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the twitter embeds are formatted correctly for InstantArticles
     * with new line between blockquote and the p tag
     *
     * @return void
     */
    public function testFormatTwitterWithNewLineBetweenBlockquoteAndPTag()
    {
        $htmlContent = '<p>Enjoy/wince...</p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote>'."\n".'<p>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatTwitter();

        $expected = '<p>Enjoy/wince...</p><figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote>'."\n".'<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure><a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a>
            <figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the twitter embeds are formatted correctly for InstantArticles
     * with spaces between blockquote and the p tag
     *
     * @return void
     */
    public function testFormatTwitterWithSpacesBetweenBlockquoteAndPTag()
    {
        $htmlContent = '<p>Enjoy/wince...</p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote>   <p>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatTwitter();

        $expected = '<p>Enjoy/wince...</p><figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote>   <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure><a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a>
            <figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the twitter embeds are formatted correctly for InstantArticles
     * with spaces between blockquote and the p tag
     *
     * @return void
     */
    public function testFormatTwitterWithScriptBetweenHtmlTags()
    {
        $htmlContent = '<p>Enjoy/wince...</p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><h2>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></h2>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><div><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></div>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatTwitter();

        $expected = '<p>Enjoy/wince...</p><figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure><a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a>
            <figure class="op-social"><iframe><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the twitter format embeds function for InstantArticles
     * without or with similar twitter info.
     *
     * @return void
     */
    public function testFormatTwitterNoneTwitterContent()
    {
        $htmlContent = '
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
            <p>He chose the Stark sigil as a tribute to... A very well know character.</p>
            <p><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatTwitter();

        $expected = '<p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
            <p>He chose the Stark sigil as a tribute to... A very well know character.</p>
            <p><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
