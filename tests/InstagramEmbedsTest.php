<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class InstagramEmbedsTest extends TestCase
{

    /**
     * Test the instagram embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatInstagram()
    {
        $htmlContent = '<h3>Nate I will see you again.</h3>
            <p>Conor McGregor has delivered a typically bullish response to his defeat to Nate Diaz in his first Instagram post since last nights fight.</p>
            <p>The Notorious thanked his fans and had a pop at his haters in a post
            that was very much true to form and illustrated that hes already thinking of
            bouncing back from what was a pretty big setback.</p>
            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
            <div style="padding:8px;"><div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
            <div style=" background:url(data:image/png;base64,iVBORw0KSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
            </div><p style=" margin:8px 0 0 0; padding:0 4px;">
            <a href="https://www.instagram.com/p/BCoCsjbLztN/" style="color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">
            Aldo you are a pussy. Dos anjos you are a pussy. When the history books are written, I showed up.
            </a></p><p style=" color:#c9c8cd; font-rap;">A photo posted by Conor McGregor Official (@thenotoriousmma) on
            <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-03-06T20:52:13+00:00">Mar 6, 2016 at 12:52pm PST</time></p>
            </div></blockquote><p><script async defer src="//platform.instagram.com/en_US/embeds.js"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatInstagram();

        $expected = '<h3>Nate I will see you again.</h3>
            <p>Conor McGregor has delivered a typically bullish response to his defeat to Nate Diaz in his first Instagram post since last nights fight.</p>
            <p>The Notorious thanked his fans and had a pop at his haters in a post
            that was very much true to form and illustrated that hes already thinking of
            bouncing back from what was a pretty big setback.</p>
            <figure class="op-social"><iframe><blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
            <div style="padding:8px;"><div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
            <div style=" background:url(data:image/png;base64,iVBORw0KSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
            </div><p style=" margin:8px 0 0 0; padding:0 4px;">
            <a href="https://www.instagram.com/p/BCoCsjbLztN/" style="color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">
            Aldo you are a pussy. Dos anjos you are a pussy. When the history books are written, I showed up.
            </a></p><p style=" color:#c9c8cd; font-rap;">A photo posted by Conor McGregor Official (@thenotoriousmma) on
            <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-03-06T20:52:13+00:00">Mar 6, 2016 at 12:52pm PST</time></p>
            </div></blockquote><script async defer src="//platform.instagram.com/en_US/embeds.js"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the instagram format embeds function for InstantArticles
     * without or with similar instagram info.
     *
     * @return void
     */
    public function testFormatInstagramForInstantArticleNoneInstagramContent()
    {
        $htmlContent = '<p>Enjoy/wince...</p><blockquote class="instagram-media"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><p>
            <script async src="//platform.vine.co/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatVine();

        $expected = '<p>Enjoy/wince...</p><blockquote class="instagram-media"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><p>
            <script async src="//platform.vine.co/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
