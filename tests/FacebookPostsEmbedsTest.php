<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class FacebookPostsEmbedsTest extends TestCase
{

    /**
     * Test the facebook post embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatFacebookPost()
    {
        $htmlContent = '<p>Weve a suspicion that his reaction was deliberately over the top,'.
            ' but the video has spread like wildfire since over the weekend, with just under'.
            ' 400,000 views, 3,500 shares, 8,500 likes and around 2,000 comments at the time of writing.</p>'.
            '<p>&nbsp;</p><div class="fb-post" data-href="https://www.facebook.com/fabud.comedianii/posts/932092330216374"'.
            ' data-width="500"></div><p>Still no word on whether he got the hat back, mind.</p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatFacebookPost();

        $expected = '<p>Weve a suspicion that his reaction was deliberately over the top,'.
            ' but the video has spread like wildfire since over the weekend, with just under'.
            ' 400,000 views, 3,500 shares, 8,500 likes and around 2,000 comments at the time of writing.</p>'.
            '<p>&nbsp;</p><figure class="op-social"><iframe><div class="fb-post" data-href="https://www.facebook.com/fabud.comedianii/posts/932092330216374"'.
            ' data-width="500"></div></iframe></figure><p>Still no word on whether he got the hat back, mind.</p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the facebook post embeds are formatted correctly for InstantArticles
     * when the content have multiple facebook image embeds
     *
     * @return void
     */
    public function testFormatMultipleFacebookPost()
    {
        $htmlContent = '
            <p><strong><em>Geordie Shore</em> star Holly Hagan has hit back at recent criticism after undergoing plastic surgery.'.
            ' </strong></p><p>Holly, along with best friend Charlotte Crosby, have made it no secret that they\'ve had work done'.
            ' to their faces and people have been quick to share their opinion about it.</p><p>She took to Facebook to tell her fans'.
            ' that even though she\'s been criticised by the public for having plastic surgery she is happy with how she looks.</p>'.
            '<p>She said, "Well YES I\'ve had fillers, YES I\'ve lost weight and YES I grew through puberty, but you know what, I LIKE'.
            ' how I look so thanks for the concern."</p><p><script>// <![CDATA[(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0];'.
            ' if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";'.
            ' fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));// ]]></script></p><div class="fb-post"'.
            ' data-href="https://www.facebook.com/ReaHollyHagan/posts/1012266155494608:0" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote'.
            ' cite="https://www.facebook.com/ReaHollyHagan/posts/1012266155494608:0"><p>The press seems to think because 1 person comments on Instagram'.
            ' that I\'ve ruined my face, then that\'s the truth. Well...</p><p>Posted by <a href="https://www.facebook.com/ReaHollyHagan/">Holly Hagan</a>'.
            ' on <a href="https://www.facebook.com/ReaHollyHagan/posts/1012266155494608:0">Friday, 11 March 2016</a></p></blockquote></div></div><p>She'.
            ' also shared an image of herself and Charlotte Crosby.</p><div id="fb-root"></div><p><script>// <![CDATA[(function(d, s, id)'.
            ' { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));// ]]>'.
            '</script></p><div class="fb-post" data-href="https://www.facebook.com/ReaHollyHagan/posts/1013789765342247:0" data-width="500">'.
            '<div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/ReaHollyHagan/posts/1013789765342247:0">'.
            '<p>She my friend Charlotte CrosbyNo airbrushing, I just lost weight thanks &gt; http://www.hollysbodybible.com</p><p>Posted by '.
            '<a href="https://www.facebook.com/ReaHollyHagan/">Holly Hagan</a> on <a href="https://www.facebook.com/ReaHollyHagan/posts/1013789765342247:0">'.
            'Sunday, 13 March 2016</a></p></blockquote></div></div><p><em>Lead image via Facebook/<a href="https://www.facebook.com/ReaHollyHagan/?fref=nf">'.
            'HollyHagan</a></em></p><div class="article-standard  visible-xs" id="mid-article-unit"><h6>Advertisement</h6><div id="div-gpt-article-middle"'.
            ' class="mpu-others" data-page="article" data-article="282998" data-tags="\'celeb-2\',\'geordie-shore\'" data-section="\'celeb\',\'movies-tv\'"></div></div>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatFacebookPost($htmlContent);

        $expected = '<p><strong><em>Geordie Shore</em> star Holly Hagan has hit back at recent criticism after undergoing plastic surgery.'.
            ' </strong></p><p>Holly, along with best friend Charlotte Crosby, have made it no secret that they\'ve had work done'.
            ' to their faces and people have been quick to share their opinion about it.</p><p>She took to Facebook to tell her fans'.
            ' that even though she\'s been criticised by the public for having plastic surgery she is happy with how she looks.</p>'.
            '<p>She said, "Well YES I\'ve had fillers, YES I\'ve lost weight and YES I grew through puberty, but you know what, I LIKE'.
            ' how I look so thanks for the concern."</p><p><script>// <![CDATA[(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0];'.
            ' if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";'.
            ' fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));// ]]></script></p><figure class="op-social"><iframe><div class="fb-post"'.
            ' data-href="https://www.facebook.com/ReaHollyHagan/posts/1012266155494608:0" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote'.
            ' cite="https://www.facebook.com/ReaHollyHagan/posts/1012266155494608:0"><p>The press seems to think because 1 person comments on Instagram'.
            ' that I\'ve ruined my face, then that\'s the truth. Well...</p><p>Posted by <a href="https://www.facebook.com/ReaHollyHagan/">Holly Hagan</a>'.
            ' on <a href="https://www.facebook.com/ReaHollyHagan/posts/1012266155494608:0">Friday, 11 March 2016</a></p></blockquote></div></div></iframe></figure><p>She'.
            ' also shared an image of herself and Charlotte Crosby.</p><div id="fb-root"></div><p><script>// <![CDATA[(function(d, s, id)'.
            ' { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));// ]]>'.
            '</script></p><figure class="op-social"><iframe><div class="fb-post" data-href="https://www.facebook.com/ReaHollyHagan/posts/1013789765342247:0" data-width="500">'.
            '<div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/ReaHollyHagan/posts/1013789765342247:0">'.
            '<p>She my friend Charlotte CrosbyNo airbrushing, I just lost weight thanks &gt; http://www.hollysbodybible.com</p><p>Posted by '.
            '<a href="https://www.facebook.com/ReaHollyHagan/">Holly Hagan</a> on <a href="https://www.facebook.com/ReaHollyHagan/posts/1013789765342247:0">'.
            'Sunday, 13 March 2016</a></p></blockquote></div></div></iframe></figure><p><em>Lead image via Facebook/<a href="https://www.facebook.com/ReaHollyHagan/?fref=nf">'.
            'HollyHagan</a></em></p><div class="article-standard  visible-xs" id="mid-article-unit"><h6>Advertisement</h6><div id="div-gpt-article-middle"'.
            ' class="mpu-others" data-page="article" data-article="282998" data-tags="\'celeb-2\',\'geordie-shore\'" data-section="\'celeb\',\'movies-tv\'"></div></div>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the facebook post format embeds function for InstantArticles
     * without or with similar facebook post info.
     * This test is perfect because fb-videos could bring the a div
     * Exactly as the one used for fb posts (div class=fb-post)
     *
     * @return void
     */
    public function testFormatFacebookPostForInstantArticleNoneFacebookPost()
    {
        $htmlContent = '<h3>I am never coming to Limerick again WTF!</h3>'.
            '<p>You may or may not have heard of David Omatade, but if you havent, he a Nigerian comedian'.
            'based in Dublin who came to a lot of peoples attention earlier this year when he posted'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/vob.100002468470953/898021350290139/?type=2&amp;theater"'.
            'target="_blank">a video of a magic trick</a> that was viewed almost 37 million times on Facebook.</p>'.
            '<p>Another of Omatades videos is currently getting a lot of attention, although probably not for the right'.
            'reasons as far as he is concerned.</p><p>Omatade was out and about in Limerick at the weekend and while'.
            'filming some interaction with locals, some cheeky pup robbed his hat and scarpered while Omatade gave chase.</p>'.
            '<blockquote id="fb-root"><p><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id))'.
            'return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";'
            .' fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script></p></blockquote>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatFacebookPost();

        $expected = '<h3>I am never coming to Limerick again WTF!</h3>'.
            '<p>You may or may not have heard of David Omatade, but if you havent, he a Nigerian comedian'.
            'based in Dublin who came to a lot of peoples attention earlier this year when he posted'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/vob.100002468470953/898021350290139/?type=2&amp;theater"'.
            ' target="_blank">a video of a magic trick</a> that was viewed almost 37 million times on Facebook.</p>'.
            '<p>Another of Omatades videos is currently getting a lot of attention, although probably not for the right'.
            'reasons as far as he is concerned.</p><p>Omatade was out and about in Limerick at the weekend and while'.
            'filming some interaction with locals, some cheeky pup robbed his hat and scarpered while Omatade gave chase.</p>'.
            '<blockquote id="fb-root"><p><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id))'.
            'return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";'
            .' fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script></p></blockquote>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
