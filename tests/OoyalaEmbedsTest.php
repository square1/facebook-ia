<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class OoyalaEmbedsTest extends TestCase
{

    /**
     * Test the formatting of Ooyala embeds
     *
     * @return void
     */
    public function testFormatOoyala()
    {
        $htmlContent = '<p><script height="422px" width="750px" src="http://player.ooyala.com/iframe.js#pbid=6311'
            .'43aa3982447caf394a185941cb69&amp;ec=Z2YnU4MjE6-1_P89ux66aQmxxUyNYopr"></script></p>'
            .'<p><script height="422px" width="750px" src="http://player.ooyala.com/iframe.js#pbid=6311'
            .'43aa3982447caf394a185941cb69&amp;ec=Z2YnU4MjE6-1_P89ux66aQmxxUyNYopr"></script></p>'
            .'<p><script height="422px" width="750px" src="http://player.ooyala.com/iframe.js#pbid=6311'
            .'43aa3982447caf394a185941cb69&amp;ec=Z2YnU4MjE6-1_P89ux66aQmxxUyNYopr"></script></p>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatOoyala();
        $expected = '<p><figure class="op-social"><iframe><script height="422px" width="750px" src="http://player.ooyala.com/iframe.js#pbid=6311'
            .'43aa3982447caf394a185941cb69&amp;ec=Z2YnU4MjE6-1_P89ux66aQmxxUyNYopr"></script></iframe></figure></p>'
            .'<p><figure class="op-social"><iframe><script height="422px" width="750px" src="http://player.ooyala.com/iframe.js#pbid=6311'
            .'43aa3982447caf394a185941cb69&amp;ec=Z2YnU4MjE6-1_P89ux66aQmxxUyNYopr"></script></iframe></figure></p>'
            .'<p><figure class="op-social"><iframe><script height="422px" width="750px" src="http://player.ooyala.com/iframe.js#pbid=6311'
            .'43aa3982447caf394a185941cb69&amp;ec=Z2YnU4MjE6-1_P89ux66aQmxxUyNYopr"></script></iframe></figure></p>';
            // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
