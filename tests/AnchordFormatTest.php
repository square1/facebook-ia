<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class AnchordFormatTest extends TestCase
{

    /**
     * Test formatting content anchord tags inside strong tags
     * anchord tags inside strong tags have to be ignored
     *
     * @return void
     */
    public function testFormatContentAnchordTagsInsideStrongTags()
    {
        $htmlContent = '<blockquote> <strong> <p>Shane is just an amazing fella. '.
        'There&rsquo;s not a player in Tipperary can catch a ball like Shane can.</p>'.
        '<p>He&rsquo;s beaten every centre back he&rsquo;s played this year.</p>'.
        '<p>To come out and do what he did today, I don&rsquo;t think anyone else could have done it.</p>'.
        '<p>He was very emotional beforehand &ndash; he&rsquo;d normally be the craic at the back of the bus, he sat on his own today.'.
        ' His performance was something else.</p> </strong> </blockquote>'.
        '<strong>Watch: '.
        '<a href="http://www.balls.ie/gaa/ardscoil-ris-manager-gives-quotes-for-the-ages-about-players-before-schools-hurling-final/328888">'.
        'Ardscoil R&iacute;s Manager Gives Quote For The Ages About Players Before Schools Hurling Final</a></strong>';
        $expected = '<blockquote> <strong> <p>Shane is just an amazing fella. '.
        'There&rsquo;s not a player in Tipperary can catch a ball like Shane can.</p>'.
        '<p>He&rsquo;s beaten every centre back he&rsquo;s played this year.</p>'.
        '<p>To come out and do what he did today, I don&rsquo;t think anyone else could have done it.</p>'.
        '<p>He was very emotional beforehand &ndash; he&rsquo;d normally be the craic at the back of the bus, he sat on his own today.'.
        ' His performance was something else.</p> </strong> </blockquote>'.
        '<strong>Watch: '.
        '<a href="http://www.balls.ie/gaa/ardscoil-ris-manager-gives-quotes-for-the-ages-about-players-before-schools-hurling-final/328888">'.
        'Ardscoil R&iacute;s Manager Gives Quote For The Ages About Players Before Schools Hurling Final</a></strong>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentAnchords();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting anchord tags inside embeds
     * anchord tags inside embeds have to be ignored
     *
     * @return void
     */
    public function testFormatContentAnchordTagsInsideEmbeds()
    {
        $htmlContent = '<figure class="op-social"><iframe><blockquote class="twitter-tweet">'.
        '<p lang="en" dir="ltr">Great picture of Dearbhail Savage and her gold medal! '.
        '<a href="https://twitter.com/SOIreland">@SOIreland</a>'.
        '<a href="https://twitter.com/hashtag/TeamIreland?src=hash">#TeamIreland</a>'.
        '<a href="http://t.co/7LTGCYeZwk">pic.twitter.com/7LTGCYeZwk</a></p>'.
        '<p>&mdash; Today FM (@todayfm) <a href="https://twitter.com/todayfm/status/626653972162547712">July 30, 2015</a>'.
        '</p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>';
        $expected = '<figure class="op-social"><iframe><blockquote class="twitter-tweet">'.
        '<p lang="en" dir="ltr">Great picture of Dearbhail Savage and her gold medal! '.
        '<a href="https://twitter.com/SOIreland">@SOIreland</a>'.
        '<a href="https://twitter.com/hashtag/TeamIreland?src=hash">#TeamIreland</a>'.
        '<a href="http://t.co/7LTGCYeZwk">pic.twitter.com/7LTGCYeZwk</a></p>'.
        '<p>&mdash; Today FM (@todayfm) <a href="https://twitter.com/todayfm/status/626653972162547712">July 30, 2015</a>'.
        '</p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentAnchords();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting anchord tags outside of strong tags and embeds
     *
     * @return void
     */
    public function testFormatContentAnchordTags()
    {
        $htmlContent = '<p>The centre-forward scored a late goal for Abbey CBS which secured victory.</p>'.
        '<p>Afterwards, selector Darragh O\'Donovan, speaking to the '.
        '<a href="http://www.irishexaminer.com/sport/gaa/hurling/shane-power-propels-abbey-cbs-to-historic-win-389909.html">'.
        'Irish Examiner</a>, hailed Power\'s performance in the face of the grief which he had just experienced.</p>';
        $expected = '<p>The centre-forward scored a late goal for Abbey CBS which secured victory.</p>'.
        '<p>Afterwards, selector Darragh O\'Donovan, speaking to the '.
        '<a href="http://www.irishexaminer.com/sport/gaa/hurling/shane-power-propels-abbey-cbs-to-historic-win-389909.html">'.
        '<strong>Irish Examiner</strong></a>, hailed Power\'s performance in the face of the grief which he had just experienced.</p>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentAnchords();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting anchord tags inside and outside of embeds
     *
     * @return void
     */
    public function testFormatContentAnchordTagsInsideEmbedsAndOutsideEmbeds()
    {
        $htmlContent = '<figure class="op-social"><iframe><blockquote class="twitter-tweet">'.
        '<p lang="en" dir="ltr">Great picture of Dearbhail Savage and her gold medal! '.
        '<a href="https://twitter.com/SOIreland">@SOIreland</a>'.
        '<a href="https://twitter.com/hashtag/TeamIreland?src=hash">#TeamIreland</a>'.
        '<a href="http://t.co/7LTGCYeZwk">pic.twitter.com/7LTGCYeZwk</a></p>'.
        '<p>&mdash; Today FM (@todayfm) <a href="https://twitter.com/todayfm/status/626653972162547712">July 30, 2015</a>'.
        '</p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>'.
        '<p>The centre-forward scored a late goal for Abbey CBS which secured victory.</p>'.
        '<p>Afterwards, selector Darragh O\'Donovan, speaking to the '.
        '<a href="http://www.irishexaminer.com/sport/gaa/hurling/shane-power-propels-abbey-cbs-to-historic-win-389909.html">'.
        'Irish Examiner</a>, hailed Power\'s performance in the face of the grief which he had just experienced.</p>';
        $expected = '<figure class="op-social"><iframe><blockquote class="twitter-tweet">'.
        '<p lang="en" dir="ltr">Great picture of Dearbhail Savage and her gold medal! '.
        '<a href="https://twitter.com/SOIreland">@SOIreland</a>'.
        '<a href="https://twitter.com/hashtag/TeamIreland?src=hash">#TeamIreland</a>'.
        '<a href="http://t.co/7LTGCYeZwk">pic.twitter.com/7LTGCYeZwk</a></p>'.
        '<p>&mdash; Today FM (@todayfm) <a href="https://twitter.com/todayfm/status/626653972162547712">July 30, 2015</a>'.
        '</p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></iframe></figure>'.
        '<p>The centre-forward scored a late goal for Abbey CBS which secured victory.</p>'.
        '<p>Afterwards, selector Darragh O\'Donovan, speaking to the '.
        '<a href="http://www.irishexaminer.com/sport/gaa/hurling/shane-power-propels-abbey-cbs-to-historic-win-389909.html">'.
        '<strong>Irish Examiner</strong></a>, hailed Power\'s performance in the face of the grief which he had just experienced.</p>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentAnchords();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting anchord tags inside and outside of strong tags
     *
     * @return void
     */
    public function testFormatContentAnchordTagsInsideStrongTagsAndOutsideStrongTags()
    {
        $htmlContent = '<blockquote><strong><p>Shane is just an amazing fella. '.
        'There&rsquo;s not a player in Tipperary can catch a ball like Shane can.</p>'.
        '<p>He&rsquo;s beaten every centre back he&rsquo;s played this year.</p>'.
        '<p>To come out and do what he did today, I don&rsquo;t think anyone else could have done it.</p>'.
        '<p>He was very emotional beforehand &ndash; he&rsquo;d normally be the craic at the back of the bus, he sat on his own today.'.
        ' His performance was something else.</p></strong></blockquote>'.
        '<strong>Watch: '.
        '<a href="http://www.balls.ie/gaa/ardscoil-ris-manager-gives-quotes-for-the-ages-about-players-before-schools-hurling-final/328888">'.
        'Ardscoil R&iacute;s Manager Gives Quote For The Ages About Players Before Schools Hurling Final</a></strong>'.
        '<p>The centre-forward scored a late goal for Abbey CBS which secured victory.</p>'.
        '<p>Afterwards, selector Darragh O\'Donovan, speaking to the '.
        '<a href="http://www.irishexaminer.com/sport/gaa/hurling/shane-power-propels-abbey-cbs-to-historic-win-389909.html">'.
        'Irish Examiner</a>, hailed Power\'s performance in the face of the grief which he had just experienced.</p>';
        $expected = '<blockquote><strong><p>Shane is just an amazing fella. '.
        'There&rsquo;s not a player in Tipperary can catch a ball like Shane can.</p>'.
        '<p>He&rsquo;s beaten every centre back he&rsquo;s played this year.</p>'.
        '<p>To come out and do what he did today, I don&rsquo;t think anyone else could have done it.</p>'.
        '<p>He was very emotional beforehand &ndash; he&rsquo;d normally be the craic at the back of the bus, he sat on his own today.'.
        ' His performance was something else.</p></strong></blockquote>'.
        '<strong>Watch: '.
        '<a href="http://www.balls.ie/gaa/ardscoil-ris-manager-gives-quotes-for-the-ages-about-players-before-schools-hurling-final/328888">'.
        'Ardscoil R&iacute;s Manager Gives Quote For The Ages About Players Before Schools Hurling Final</a></strong>'.
        '<p>The centre-forward scored a late goal for Abbey CBS which secured victory.</p>'.
        '<p>Afterwards, selector Darragh O\'Donovan, speaking to the '.
        '<a href="http://www.irishexaminer.com/sport/gaa/hurling/shane-power-propels-abbey-cbs-to-historic-win-389909.html">'.
        '<strong>Irish Examiner</strong></a>, hailed Power\'s performance in the face of the grief which he had just experienced.</p>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentAnchords();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

}