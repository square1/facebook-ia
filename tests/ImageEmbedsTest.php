<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class ImageEmbedsTest extends TestCase
{

    /**
     * Test removing wrapping tags for img
     * This test will check the function without img tags on the content
     *
     * @return void
     */
    public function testRemovingHtmlAroundImagesWithoutImages()
    {
        $htmlContent = "<p>This large yet lightweight changing bag was no doubt designed with today's fashion conscious and busy Mums in mind.</p><p>Not only does the three main compartments help to keep clothes, nappies and food apart, each compartment also has additional pockets allowing you to be totally organised. And, you know, who doesn't want that?</p><p>Oh, and the two outside zipped pockets are perfect for personal items that you need quick access too, whil the buggy clips, a large wipe-clean change mat and two insulated bottle pockets are just icing on the already great cake.</p><p><em>Available from Happybags.co.uk for 86<br></em></p>";

        $expected = $htmlContent;

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeHtmlAroundImages();


        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test removing wrapping tags for img
     * This test will check the function with a img tag wrapped by
     * <p class="p1"><a href***>
     *
     * @return void
     */
    public function testRemovingHtmlAroundImagesWithPClassAndATag()
    {
        $htmlContent = '<a href="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" rel="attachment wp-att-238092"><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500" /></a><p class="p1">Maternity jeans 14.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank">New Look</a>; maternity jeans22.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-black-underbump-jeggings-_326137601" target="_blank">';

        $expected = '<img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"><p class="p1">Maternity jeans 14.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank">New Look</a>; maternity jeans22.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-black-underbump-jeggings-_326137601" target="_blank"></a></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeHtmlAroundImages();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test removing wrapping tags for img
     * This test will check the function with a img tag wrapped by <p>
     * and multiple imgs
     *
     * @return void
     */
    public function testRemovingHtmlAroundImagesWithPTagAndMultipleImgs()
    {
        $htmlContent = '<img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500" /><p class="p1">Maternity jeans 14.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank">New Look</a></p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500" />; maternity jeans22.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-black-underbump-jeggings-_326137601" target="_blank"><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500" />';

        $expected = '<img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"><p class="p1">Maternity jeans 14.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank">New Look</a></p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500">; maternity jeans22.99 from <img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500">';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeHtmlAroundImages();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
