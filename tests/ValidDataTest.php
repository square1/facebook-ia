<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class ValidDataTest extends TestCase
{

    /**
     * Test the given content is a vaild string
     *
     * @return void()
     */
    public function testIfGivenContentIsValid()
    {
        $content = "<p>this is a valid string</p>";
        $formatter = $this->getFormatterInstance($content);
        $result = $formatter->setWellFormattedHTMLContent();
        $this->assertEquals($result, $content);
    }

    /**
     * Test the given content and ur are vaild strings
     *
     * @return void()
     */
    public function testIfGivenUrlAndContentAreValid()
    {
        $content = "<p>this is a valid string</p>";
        $url = "this-is-a-valid-url";
        $formatter = $this->getFormatterInstance($content, $url);
        $result = $formatter->setWellFormattedHTMLContent();
        $this->assertEquals($result, $content);
    }

    /**
     * Test the exception if bad contend is used
     *
     * @return void()
     */
    public function testExceptionWhenBadContent()
    {
        $content = array('content' => 'some data');
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Html Content must be string.');
        $formatter = $this->getFormatterInstance($content);
    }

    /**
     * Test the exception if empty contend is used
     *
     * @return void()
     */
    public function testExceptionWhenEmptyContent()
    {
        $content = '';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Html Content can\'t be empty or null.');
        $formatter = $this->getFormatterInstance($content);
    }

    /**
     * Test the exception if bad url is used
     *
     * @return void()
     */
    public function testExceptionWhenBadUrl()
    {
        $content = 'some valid content';
        $url = array('bad url');
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Only strings are allowed.');
        $formatter = $this->getFormatterInstance($content, $url);
    }
}
