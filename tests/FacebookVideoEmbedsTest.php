<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class FacebookVideoEmbedsTest extends TestCase
{

    /**
     * Test the facebook video embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatFacebookVideo()
    {
        $htmlContent = '<h3>I am never coming to Limerick again WTF!</h3>'.
            '<p>You may or may not have heard of David Omatade, but if you havent, he a Nigerian comedian'.
            'based in Dublin who came to a lot of peoples attention earlier this year when he posted'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/vob.100002468470953/898021350290139/?type=2&amp;theater"'.
            ' target="_blank">a video of a magic trick</a> that was viewed almost 37 million times on Facebook.</p>'.
            '<p>Another of Omatades videos is currently getting a lot of attention, although probably not for the right'.
            'reasons as far as he is concerned.</p><p>Omatade was out and about in Limerick at the weekend and while'.
            'filming some interaction with locals, some cheeky pup robbed his hat and scarpered while Omatade gave chase.</p>'.
            '<div id="fb-root"></div><p><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id))'.
            'return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; '.
            'fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));'.
            '</script></p><div class="fb-post" data-href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/"'.
            ' data-width="500"><div class="fb-xfbml-parse-ignore">'.
            '<blockquote cite="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">'.
            '<p>I am never coming to Limerick again WTF!Follow David Omotade for more videos</p>'.
            '<p>Posted by <a href="https://www.facebook.com/fabud.comedianii">David Omotade</a> on'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">Saturday, 21 November 2015</a></p>'.
            '</blockquote></div></div>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatFacebookVideo();

        $expected = '<h3>I am never coming to Limerick again WTF!</h3>'.
            '<p>You may or may not have heard of David Omatade, but if you havent, he a Nigerian comedian'.
            'based in Dublin who came to a lot of peoples attention earlier this year when he posted'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/vob.100002468470953/898021350290139/?type=2&amp;theater"'.
            ' target="_blank">a video of a magic trick</a> that was viewed almost 37 million times on Facebook.</p>'.
            '<p>Another of Omatades videos is currently getting a lot of attention, although probably not for the right'.
            'reasons as far as he is concerned.</p><p>Omatade was out and about in Limerick at the weekend and while'.
            'filming some interaction with locals, some cheeky pup robbed his hat and scarpered while Omatade gave chase.</p>'.
            '<figure class="op-social"><iframe>'.
            '<div id="fb-root"></div><p><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id))'.
            'return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; '.
            'fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));'.
            '</script></p><div class="fb-post" data-href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/"'.
            ' data-width="500"><div class="fb-xfbml-parse-ignore">'.
            '<blockquote cite="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">'.
            '<p>I am never coming to Limerick again WTF!Follow David Omotade for more videos</p>'.
            '<p>Posted by <a href="https://www.facebook.com/fabud.comedianii">David Omotade</a> on'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">Saturday, 21 November 2015</a></p>'.
            '</blockquote></div></div></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the facebook video format embeds function for InstantArticles
     * without or with similar facebook video info.
     *
     * @return void
     */
    public function testFormatFacebookVideoForInstantArticleNoneFacebookVideo()
    {
        $htmlContent = '<h3>I am never coming to Limerick again WTF!</h3>'.
            '<p>You may or may not have heard of David Omatade, but if you havent, he a Nigerian comedian'.
            'based in Dublin who came to a lot of peoples attention earlier this year when he posted'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/vob.100002468470953/898021350290139/?type=2" target="_blank">a video of a'.
            'magic trick</a> that was viewed almost 37 million times on Facebook.'.
            '</p>'.
            '<p>Another of Omatades videos is currently getting a lot of attention, although probably not for the right'.
            'reasons as far as he is concerned.</p>'.
            '<p>Omatade was out and about in Limerick at the weekend and while'.
            'filming some interaction with locals, some cheeky pup robbed his hat and scarpered while Omatade gave chase.</p>'.
            '<p>'.
                '<script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id))'.
                'return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.'.
                'insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));'.
                '</script>'.
            '</p>'.
            '<div class="fb-post" data-href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/" data-width="500">'.
                '<div class="fb-xfbml-parse-ignore">'.
                    '<blockquote cite="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">'.
                    '<p>I am never coming to Limerick again WTF!Follow David Omotade for more videos</p>'.
                    '<p>Posted by <a href="https://www.facebook.com/fabud.comedianii">David Omotade</a> on'.
                    '<a href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">Saturday, 21 November 2015</a>'.
                    '</p>'.
                    '</blockquote>'.
                '</div>'.
            '</div>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatFacebookVideo();

        $expected = '<h3>I am never coming to Limerick again WTF!</h3>'.
            '<p>You may or may not have heard of David Omatade, but if you havent, he a Nigerian comedian'.
            'based in Dublin who came to a lot of peoples attention earlier this year when he posted'.
            '<a href="https://www.facebook.com/fabud.comedianii/videos/vob.100002468470953/898021350290139/?type=2" target="_blank">a video of a'.
            'magic trick</a> that was viewed almost 37 million times on Facebook.'.
            '</p>'.
            '<p>Another of Omatades videos is currently getting a lot of attention, although probably not for the right'.
            'reasons as far as he is concerned.</p>'.
            '<p>Omatade was out and about in Limerick at the weekend and while'.
            'filming some interaction with locals, some cheeky pup robbed his hat and scarpered while Omatade gave chase.</p>'.
            '<p>'.
                '<script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id))'.
                'return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.'.
                'insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));'.
                '</script>'.
            '</p>'.
            '<div class="fb-post" data-href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/" data-width="500">'.
                '<div class="fb-xfbml-parse-ignore">'.
                    '<blockquote cite="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">'.
                    '<p>I am never coming to Limerick again WTF!Follow David Omotade for more videos</p>'.
                    '<p>Posted by <a href="https://www.facebook.com/fabud.comedianii">David Omotade</a> on'.
                    '<a href="https://www.facebook.com/fabud.comedianii/videos/932062750219332/">Saturday, 21 November 2015</a>'.
                    '</p>'.
                    '</blockquote>'.
                '</div>'.
            '</div>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
