<?php

namespace Tests;

use Square1\Formatter\Formatter as Formatter;

require_once(__DIR__ . '/../bootstrap/start.php');

/**
* Base test class
*/
class TestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * get a Formatter Class instance
     *
     * @param  string $content Html Content
     * @param  string $postUrl Post seo url
     *
     * @return [type]          [description]
     */
    public function getFormatterInstance($content, $postUrl = null)
    {
        return new Formatter($content, $postUrl);
    }
}
