<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class BlockquotesEmbedsTest extends TestCase
{

    /**
     * Test formatting content blockquotes without social embeds blockquotes
     *
     * @return void
     */
    public function testFormatContentBlockquotesWithoutSocialEmbedsBlockquotes()
    {
        $htmlContent = '<blockquote><p>I think probably the most <a href="">underrated player I'.
        ' ever played with.</a></p><p>Left or right, <a href="">free-kicks... </a>He was a bit unfortunate he played '.
        'with Beckham who took a lot of the free-kicks, he was good at them, Denis. You didn\'t want to do finishing '.
        '[training] with Denis, he would embarrass the forwards, he was that good on either foot.</p><p>An exceptional'.
        ' player. Good lad</p></blockquote>';
        $expected = '<br><blockquote><strong><p>I think probably the most <a href="">underrated player I ever played'.
        ' with.</a></p><p>Left or right, <a href="">free-kicks... </a>He was a bit unfortunate he played with Beckham '.
        'who took a lot of the free-kicks, he was good at them, Denis. You didn\'t want to do finishing [training] with '.
        'Denis, he would embarrass the forwards, he was that good on either foot.</p><p>An exceptional player. Good lad</p>'.
        '</strong></blockquote><br>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentBlockquotes();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting content blockquotes with social embeds blockquotes
     *
     * @return void
     */
    public function testFormatContentBlockquotesWithSocialEmbedsBlockquotes()
    {
        $htmlContent = '<iframe><blockquote><p>I think probably the most underrated player '.
        'I ever played with.</p><p>Left or right, <a href="">free-kicks... </a>He was a bit unfortunate he'.
        ' played with Beckham who took a lot of the free-kicks, he was good at them, Denis. You didn\'t '.
        'want to do finishing [training] with Denis, he would embarrass the forwards, he was that good on'.
        ' either foot.</p><p>An exceptional player. Good lad</p></blockquote></iframe>'.
        '<blockquote><p>I think probably the most <a href="">underrated player I ever '.
        'played with.</a></p><p>Left or right, <a href="">free-kicks... </a>He was a bit'.
        ' unfortunate he played with Beckham who took a lot of the free-kicks, he was good'.
        ' at them, Denis. You didn\'t want to do finishing [training] with Denis, he would'.
        ' embarrass the forwards, he was that good on either foot.</p><p>An exceptional player'.
        '. Good lad</p></blockquote>';
        $expected = '<iframe><blockquote><p>I think probably the most underrated player I ever played with.</p>'.
        '<p>Left or right, <a href="">free-kicks... </a>He was a bit unfortunate he played with Beckham who took'.
        ' a lot of the free-kicks, he was good at them, Denis. You didn\'t want to do finishing [training] with Denis,'.
        ' he would embarrass the forwards, he was that good on either foot.</p><p>An exceptional player. Good lad</p>'.
        '</blockquote></iframe>'.
        '<br><blockquote><strong><p>I think probably the most <a href="">underrated player I ever played with.'.
        '</a></p><p>Left or right, <a href="">free-kicks... </a>He was a bit unfortunate'.
        ' he played with Beckham who took a lot of the free-kicks, he was good at them, Denis. You didn\'t want to do'.
        ' finishing [training] with Denis, he would embarrass the forwards, he was that good on either foot.</p>'.
        '<p>An exceptional player. Good lad</p></strong></blockquote><br>';


        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentBlockquotes();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting content blockquotes with only social embeds blockquotes
     *
     * @return void
     */
    public function testFormatContentBlockquotesOnlySocialEmbedsBlockquotes()
    {
        $htmlContent = '<iframe><blockquote><p>I think probably the most underrated player I ever played'.
        ' with.</p><p>Left or right, <a href="">free-kicks... </a>He was a bit unfortunate he played with Beckham who took'.
        ' a lot of the free-kicks, he was good at them, Denis. You didn\'t want to do finishing [training] with Denis,'.
        ' he would embarrass the forwards, he was that good on either foot.</p><p>An exceptional player. Good lad</p>'.
        '</blockquote></iframe>';
        $expected = '<iframe><blockquote><p>I think probably the most underrated player I ever played with.</p><p>Left'.
        ' or right, <a href="">free-kicks... </a>He was a bit unfortunate he played with Beckham who took a lot of the'.
        ' free-kicks, he was good at them, Denis. You didn\'t want to do finishing [training] with Denis, he would'.
        ' embarrass the forwards, he was that good on either foot.</p><p>An exceptional player. Good lad</p></blockquote>'.
        '</iframe>';


        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentBlockquotes();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting content blockquotes with multiple social embeds blockquotes
     *
     * @return void
     */
    public function testFormatContentBlockquotesMultipleSocialEmbedsBlockquotes()
    {
        $htmlContent = '<p>Former Norwich and West Ham striker Dean Ashton saw an incredibly '.
        'promising career cut short through the caprice of injury back in 2009. Ashton scored 15 goals in 46 league'.
        ' appearances for West Ham - along with a goal in the 2006 FA Cup final against Liverpool - only to be forced'.
        ' to call it a day owing to an ankle injury at the age of just 26.&nbsp;Ashton sustained the injury&nbsp;during'.
        ' his only international appearance with England in 2008.</p><p>West Ham fans were given further cause'.
        ' to rue Ashton\'s injury-shortened career today, as their former striker scored this fantastic overhead'.
        ' kick during Mark Noble\'s testimonial game this afternoon:</p><figure class="op-social"><iframe>'.
        '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Dean Ashton just did this in Mark Noble\'s testimonial'.
        ' &#128563;<br><br>Take a bow son...<a href="https://twitter.com/hashtag/whufc?src=hash">#whufc</a>'.
        '<a href="https://twitter.com/hashtag/marknoble?src=hash">#marknoble</a>'.
        '<a href="https://twitter.com/hashtag/MarkNobleTestimonial?src=hash">#MarkNobleTestimonial</a>'.
        '<a href="https://t.co/Mpq4DekqA8">pic.twitter.com/Mpq4DekqA8</a></p><p>&mdash; Dangerous Attack'.
        ' Bet (@dangerousattack) <a href="https://twitter.com/dangerousattack/status/714462615867625473">'.
        'March 28, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8">'.
        '</script><p>One fan in the crowd caught a fantastic angle of the strike:</p><blockquote class="twitter-tweet">'.
        '<p lang="en" dir="ltr">Dean Ashton, get him on the plane..<a href="https://t.co/EfXkh3Vs6G">pic.twitter.com/EfXkh3Vs6G</a>'.
        '</p><p>&mdash; Soccer St. (@SoccerSt_) <a href="https://twitter.com/SoccerSt_/status/714463782060339200">March 28, 2016'.
        '</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>'.
        '<p>Noble is celebrating ten years at the club with a game between West Ham and a West Ham All Stars XI.'.
        ' Along with what might have been, Hammers fans were given a reminder of what once was, as Paolo Di Canio'.
        ' also scored a solo goal that perhaps owed more to the opposition\'s unwillingness to tackle him:</p>'.
        '<blockquote class="twitter-tweet"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Paolo?src=hash">'.
        '#Paolo</a><a href="https://twitter.com/hashtag/Di?src=hash">#Di</a>'.
        '<a href="https://twitter.com/hashtag/Canio?src=hash">#Canio</a> '.
        'scores a beauty solo goal at <a href="https://twitter.com/hashtag/Mark?src=hash">#Mark</a>'.
        '<a href="https://twitter.com/hashtag/Nobles?src=hash">#Nobles</a>'.
        '<a href="https://twitter.com/hashtag/Testimonial?src=hash">#Testimonial</a>'.
        ' at <a href="https://twitter.com/hashtag/Upton?src=hash">#Upton</a>'.
        '<a href="https://twitter.com/hashtag/Park?src=hash">#Park</a>'.
        '<a href="https://twitter.com/hashtag/MN16?src=hash">#MN16</a>'.
        '<a href="https://twitter.com/hashtag/WHUFC?src=hash">#WHUFC</a>&#9874;'.
        ' <a href="https://t.co/hEkOZkzbKZ">https://t.co/hEkOZkzbKZ</a></p>'.
        '<p>&mdash; Alex Robertson (@19ARobertson) '.
        '<a href="https://twitter.com/19ARobertson/status/714464065599512576">March 28, 2016</a>'.
        '</p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8">'.
        '</script></iframe></figure>';
        $expected = '<p>Former Norwich and West Ham striker Dean Ashton saw an incredibly '.
        'promising career cut short through the caprice of injury back in 2009. Ashton scored 15 goals in 46 league'.
        ' appearances for West Ham - along with a goal in the 2006 FA Cup final against Liverpool - only to be forced'.
        ' to call it a day owing to an ankle injury at the age of just 26.&nbsp;Ashton sustained the injury&nbsp;during'.
        ' his only international appearance with England in 2008.</p><p>West Ham fans were given further cause'.
        ' to rue Ashton\'s injury-shortened career today, as their former striker scored this fantastic overhead'.
        ' kick during Mark Noble\'s testimonial game this afternoon:</p><figure class="op-social"><iframe>'.
        '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Dean Ashton just did this in Mark Noble\'s testimonial'.
        ' &#128563;<br><br>Take a bow son...<a href="https://twitter.com/hashtag/whufc?src=hash">#whufc</a>'.
        '<a href="https://twitter.com/hashtag/marknoble?src=hash">#marknoble</a>'.
        '<a href="https://twitter.com/hashtag/MarkNobleTestimonial?src=hash">#MarkNobleTestimonial</a>'.
        '<a href="https://t.co/Mpq4DekqA8">pic.twitter.com/Mpq4DekqA8</a></p><p>&mdash; Dangerous Attack'.
        ' Bet (@dangerousattack) <a href="https://twitter.com/dangerousattack/status/714462615867625473">'.
        'March 28, 2016</a></p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8">'.
        '</script><p>One fan in the crowd caught a fantastic angle of the strike:</p><blockquote class="twitter-tweet">'.
        '<p lang="en" dir="ltr">Dean Ashton, get him on the plane..<a href="https://t.co/EfXkh3Vs6G">pic.twitter.com/EfXkh3Vs6G</a>'.
        '</p><p>&mdash; Soccer St. (@SoccerSt_) <a href="https://twitter.com/SoccerSt_/status/714463782060339200">March 28, 2016'.
        '</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>'.
        '<p>Noble is celebrating ten years at the club with a game between West Ham and a West Ham All Stars XI.'.
        ' Along with what might have been, Hammers fans were given a reminder of what once was, as Paolo Di Canio'.
        ' also scored a solo goal that perhaps owed more to the opposition\'s unwillingness to tackle him:</p>'.
        '<blockquote class="twitter-tweet"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Paolo?src=hash">'.
        '#Paolo</a><a href="https://twitter.com/hashtag/Di?src=hash">#Di</a>'.
        '<a href="https://twitter.com/hashtag/Canio?src=hash">#Canio</a> '.
        'scores a beauty solo goal at <a href="https://twitter.com/hashtag/Mark?src=hash">#Mark</a>'.
        '<a href="https://twitter.com/hashtag/Nobles?src=hash">#Nobles</a>'.
        '<a href="https://twitter.com/hashtag/Testimonial?src=hash">#Testimonial</a>'.
        ' at <a href="https://twitter.com/hashtag/Upton?src=hash">#Upton</a>'.
        '<a href="https://twitter.com/hashtag/Park?src=hash">#Park</a>'.
        '<a href="https://twitter.com/hashtag/MN16?src=hash">#MN16</a>'.
        '<a href="https://twitter.com/hashtag/WHUFC?src=hash">#WHUFC</a>&#9874;'.
        ' <a href="https://t.co/hEkOZkzbKZ">https://t.co/hEkOZkzbKZ</a></p>'.
        '<p>&mdash; Alex Robertson (@19ARobertson) '.
        '<a href="https://twitter.com/19ARobertson/status/714464065599512576">March 28, 2016</a>'.
        '</p></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8">'.
        '</script></iframe></figure>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatContentBlockquotes();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
