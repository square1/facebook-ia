<?php

namespace Square1\Formatter;

use Illuminate\Support\ServiceProvider;

/**
 * FormatterServiceProvider
 */
class FormatterServiceProvider extends ServiceProvider
{

    /**
     * Register FormatterServiceProvider
     */
    public function register()
    {
        $this->app->bind('formatter', function ($app) {
            return new Formatter();
        });
    }
}
