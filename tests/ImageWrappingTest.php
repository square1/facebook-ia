<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class ImageWrappingTest extends TestCase
{
    /**
     * Test wrapping all imgs with a figure tag with the attribute data-mode non-interactive
     * and data-feedbak fbLlikes,fbLcomments to the figure
     * This test will check a string with many img
     *
     * @return void
     */
    public function testWrappingImgWithAFigureTag()
    {
        $htmlContent = '<p>10) Moe has a "Help Wanted: No Irish Need Apply" sign</p><img class="aligncenter size-full wp-image-481271" src="http://m0.joe.ie/wp-content/uploads/2015/01/14115403/Help-Irish.jpg" alt="Help Irish" width="630" height="485" /><p><strong>Bart Star</strong></p><p>11) This chalkboard gag</p><img class="aligncenter wp-image-481272" src="http://m0.joe.ie/wp-content/uploads/2015/01/14115424/Irish-Dancing.jpg" alt="Irish Dancing" width="420" height="320" /><p><strong>Trash of the Titans</strong></p>';
        $expected = '<p>10) Moe has a "Help Wanted: No Irish Need Apply" sign</p><figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments"><img class="aligncenter size-full wp-image-481271" src="http://m0.joe.ie/wp-content/uploads/2015/01/14115403/Help-Irish.jpg" alt="Help Irish" width="630" height="485"></figure><p><strong>Bart Star</strong></p><p>11) This chalkboard gag</p><figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments"><img class="aligncenter wp-image-481272" src="http://m0.joe.ie/wp-content/uploads/2015/01/14115424/Irish-Dancing.jpg" alt="Irish Dancing" width="420" height="320"></figure><p><strong>Trash of the Titans</strong></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->wrapImgWithFigureTag();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test wrapping all imgs with a figure tag with the attribute data-mode non-interactive
     * This test will check a string with none img
     *
     * @return void
     */
    public function testWrappingImgWithAFigureTagWithNoneImgInTheContent()
    {
        $htmlContent = '<iframe style="position: absolute; top: 0; left: 0;"'.
            'src="https://gfycat.com/ifr/ThisDistinctGartersnake" width="100%" height="100%" '."\n   ".'frameborder="0" '.
            'scrolling="no" allowfullscreen="allowfullscreen"></iframe>'.
            '<iframe style="position: absolute; top: 0; left: 0;" src="https://gfycat.com/ifr/ThisDistinctGartersnake" '.
            'height="100%" width="100%" frameborder="0" scrolling="no"'.
            'allowfullscreen="allowfullscreen"></iframe>'.
            '<a href="http://m0.joe.co.uk/wp-content/uploads/2016/03/15160133/irish-gif.gif" '.
            'rel="attachment wp-att-47315"></a> via 30 Rock/NBC</div></p>';
        $expected = '<iframe style="position: absolute; top: 0; left: 0;"'.
            ' src="https://gfycat.com/ifr/ThisDistinctGartersnake" width="100%" height="100%" frameborder="0" '.
            'scrolling="no" allowfullscreen="allowfullscreen"></iframe>'.
            '<iframe style="position: absolute; top: 0; left: 0;" src="https://gfycat.com/ifr/ThisDistinctGartersnake" '.
            'height="100%" width="100%" frameborder="0" scrolling="no"'.
            ' allowfullscreen="allowfullscreen"></iframe>'.
            '<a href="http://m0.joe.co.uk/wp-content/uploads/2016/03/15160133/irish-gif.gif" '.
            'rel="attachment wp-att-47315"></a> via 30 Rock/NBC';

        $htmlContent = $this->getFormatterInstance($htmlContent)->wrapImgWithFigureTag();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
