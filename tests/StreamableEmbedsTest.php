<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class StreamableEmbedsTest extends TestCase
{

    /**
     * Test formatting Streamable embeds
     *
     * @return void
     */
    public function testStreamableEmbeds()
    {
        $htmlContent =  '<div style="width: 100%; height: 0px; position: relative; padding-bottom: 53.933%;"><iframe style="width: 100%; height: 100%; position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></div>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatStreamable();
        $expected = '<figure class="op-social"><iframe style="width: 100%; height: 100%; position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></figure>';

        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting Streamable embeds
     *
     * @return void
     */
    public function testStreamableEmbedsWithoutWidthAndHeightPercentage()
    {
        $htmlContent =  '<div style="height: 0px; position: relative; padding-bottom: 53.933%;"><iframe style="position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></div>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatStreamable();
        $expected = '<figure class="op-social"><iframe style="position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></figure>';

        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test formatting Streamable embeds using in-production html code
     *
     * @return void
     */
    public function testStreamableEmbedsWithProductionText()
    {
        $htmlContent = '<p><script async defer src="//platform.instagram.com/en_US/embeds.js"></script></p>'.
            '<p><iframe src="https://player.vimeo.com/video/158647901" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>'.
            '<p><a href="https://vimeo.com/158647901">LEVITATION</a> from <a href="https://vimeo.com/silasveta">SILA SVETA</a> on <a href="https://vimeo.com">Vimeo</a>.</p>'.
            '<p>&nbsp;</p><p>&nbsp;</p>'.
            '<p>One of the students injured in the Berkeley balcony collapse has thanked another survivor for breaking her fall in the accident.</p>'.
            '<p>Clodagh Cogley was one of the students injured after the balcony collapsed in Kittredge Street, Berkeley, yesterday.</p>'.
            '<p>Her brother Darragh Cogley has said that Clodagh is \'awake, upbeat and doing really well\'.</p>'.
            '<p>He says the family can\'t believe how lucky she is to be allright and Clodagh wishes to thank another one of the injured students, '.
            'Jack Halpin, for breaking her fall.</p><blockquote><p>She is awake, somehow upbeat and doing really well.</p>'.
            '<div style="width: 100%; height: 0px; position: relative; padding-bottom: 53.933%;">'.
            '<iframe style="width: 100%; height: 100%; position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>'.
            '</div>'
            .'<p>We can\'t believe how lucky she and the rest of the survivors were and Clodagh wanted to say particular thanks to Jack Halpin for '.
            'grabbing her and breaking her fall.</p></blockquote>'.
            '<p>Clodagh Cogley is the daughter of the former Head of Sport at RTE and current Director of Broadcast at TV3, Niall Cogley.</p>'.
            '<p>Jack Halpin is a footballer with St. Jude\'s GAA club in Tempelogue. He is still seriously injured following yesterday\'s tragedy. '.
            'The others injured are Hannah Waters, Conor Flynn, Niall Murray, Sean Fahey and Aoife Beary.</p>'.
            '<p>Her father spoke to the Independent today, describing it as a \'miracle\' how his daughter was still alive. He said she \'seemed '.
            'chirpy enough given what had happened\' but was unsure whether this was down to the medication she was on.</p>'.
            '<p><iframe src="https://vine.co/v/iHTTDHz6Z2v/embed/simple" width="600" height="600" frameborder="0"></iframe>'.
            '<script src="https://platform.vine.co/static/scripts/embed.js"></script></p>'.
            '<p></p><p>Global footer on Balls</p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatStreamable();

        $expected = '<p><script async defer src="//platform.instagram.com/en_US/embeds.js"></script></p>'.
            '<p><iframe src="https://player.vimeo.com/video/158647901" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>'.
            '<p><a href="https://vimeo.com/158647901">LEVITATION</a> from <a href="https://vimeo.com/silasveta">SILA SVETA</a> on <a href="https://vimeo.com">Vimeo</a>.</p>'.
            '<p>&nbsp;</p><p>&nbsp;</p>'.
            '<p>One of the students injured in the Berkeley balcony collapse has thanked another survivor for breaking her fall in the accident.</p>'.
            '<p>Clodagh Cogley was one of the students injured after the balcony collapsed in Kittredge Street, Berkeley, yesterday.</p>'.
            '<p>Her brother Darragh Cogley has said that Clodagh is \'awake, upbeat and doing really well\'.</p>'.
            '<p>He says the family can\'t believe how lucky she is to be allright and Clodagh wishes to thank another one of the injured students, '.
            'Jack Halpin, for breaking her fall.</p><blockquote><p>She is awake, somehow upbeat and doing really well.</p>'.
            '<figure class="op-social">'.
            '<iframe style="width: 100%; height: 100%; position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>'.
            '</figure>'
            .'<p>We can\'t believe how lucky she and the rest of the survivors were and Clodagh wanted to say particular thanks to Jack Halpin for '.
            'grabbing her and breaking her fall.</p></blockquote>'.
            '<p>Clodagh Cogley is the daughter of the former Head of Sport at RTE and current Director of Broadcast at TV3, Niall Cogley.</p>'.
            '<p>Jack Halpin is a footballer with St. Jude\'s GAA club in Tempelogue. He is still seriously injured following yesterday\'s tragedy. '.
            'The others injured are Hannah Waters, Conor Flynn, Niall Murray, Sean Fahey and Aoife Beary.</p>'.
            '<p>Her father spoke to the Independent today, describing it as a \'miracle\' how his daughter was still alive. He said she \'seemed '.
            'chirpy enough given what had happened\' but was unsure whether this was down to the medication she was on.</p>'.
            '<p><iframe src="https://vine.co/v/iHTTDHz6Z2v/embed/simple" width="600" height="600" frameborder="0"></iframe>'.
            '<script src="https://platform.vine.co/static/scripts/embed.js"></script></p>'.
            '<p></p><p>Global footer on Balls</p>';

        $this->assertEquals($expected, $htmlContent);
    }

}