<?php

namespace Tests;

/**
* Getty Images embeds tests
*/

class GettyImagesTest extends TestCase
{

	/**
     * Test the formatting of GettyImages embeds
     *
     * @return void
     */
    public function testGettyImageEmbeds()
    {
        $htmlContent = '<div class="getty embed image" style="background-color: #fff; display: inline-block; font-family:'.
        ' \'Helvetica Neue\',Helvetica,Arial,sans-serif; color: #a7a7a7; font-size: 11px; width: 100%; max-width: 594px;">'.
        '<div style="padding: 0; margin: 0; text-align: left;"><a style="color: #a7a7a7; text-decoration: none; font-weight: '.
        'normal !important; border: none; display: inline-block;" href="http://www.gettyimages.com/detail/512078752" target="_blank">'.
        'Embed from Getty Images</a></div><div style="overflow: hidden; position: relative; height: 0; padding: 67.003367% 0 0 0; '.
        'width: 100%;"><iframe style="display: inline-block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; '.
        'margin: 0;" src="//embed.gettyimages.com/embed/512078752?et=bmD1k8DiS_ht-QgO6UE_Ew&amp;viewMoreLink=off&amp;sig='.
        'BBFi_aBkbHv1-5uJXJ5n5Ffohd99KnxK8-3sweyICNM=" width="594" height="398" frameborder="0" scrolling="no"></iframe></div>'.
        '<p style="margin: 0;"></p></div>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatGetttyImages();
        $expected = '<figure class="op-social"><div class="getty embed image" style="background-color: #fff; display: inline-block; font-family:'.
        ' \'Helvetica Neue\',Helvetica,Arial,sans-serif; color: #a7a7a7; font-size: 11px; width: 100%; max-width: 594px;">'.
        '<div style="padding: 0; margin: 0; text-align: left;"><a style="color: #a7a7a7; text-decoration: none; font-weight: '.
        'normal !important; border: none; display: inline-block;" href="http://www.gettyimages.com/detail/512078752" target="_blank">'.
        'Embed from Getty Images</a></div><div style="overflow: hidden; position: relative; height: 0; padding: 67.003367% 0 0 0; '.
        'width: 100%;"><iframe style="display: inline-block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; '.
        'margin: 0;" src="//embed.gettyimages.com/embed/512078752?et=bmD1k8DiS_ht-QgO6UE_Ew&amp;viewMoreLink=off&amp;sig='.
        'BBFi_aBkbHv1-5uJXJ5n5Ffohd99KnxK8-3sweyICNM=" width="594" height="398" frameborder="0" scrolling="no"></iframe></div>'.
        '<p style="margin: 0;"></p></div></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }


}