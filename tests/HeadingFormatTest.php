<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class HeadingFormatTest extends TestCase
{

    /**
     * Test h2 html content gets wrapped with bold tags
     * in this case we are handling a plain html line
     *
     * @return void
     */
    public function testWrappingH2ContentWithBoldTagsPlainHTMLLine()
    {
        $htmlContent = '<h2>imperdiet</h2>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatHeadings();
        $expected = '<h2><b>imperdiet</b></h2>';
        $this->assertEquals($expected, $htmlContent);
    }
}
