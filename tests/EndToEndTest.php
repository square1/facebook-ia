<?php

namespace Tests;

/**
* Facebook IA html formatting Test class
*/
class EndToEndTest extends TestCase
{
    /**
     * Test if content is well formatted for Facebook IA
     * after apply all the format funcitons
     *
     * @return void
     */
    public function testIfContentIsWellFormatted()
    {
        $htmlContent = $this->getSourceHtml();
        $postUrl = 'uncategorized/survivor-clodagh-cogley-thanks-injured-gaa-player-for-breaking-her-fall-in-balcony-tragedy/297342';
        $expected = $this->getExpectedHtml();
        $htmlContent = $this->getFormattedHtml($htmlContent, $postUrl);
        $this->assertEquals($expected, $htmlContent);

    }

    /**
     * Get formatted html
     *
     * @param  array  $htmlContent Post data
     *
     * @return array $htmlContent Post data formatted
     */
    private function getFormattedHtml($htmlContent, $postUrl)
    {
        $htmlContent = $this->getFormatterInstance($htmlContent, $postUrl)->getFormattedContent();
        return $htmlContent;
    }

    /**
     * Source Html body
     *
     * @return $string Html body
     */
    private function getSourceHtml()
    {
        return '

            <p><a href="#"></a></p>'.
            '<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; '.
            'border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; '.
            'max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">'.
            '<div style="padding:8px;">'.
            '<div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;">'.
            '<div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT'.
            '0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81I'.
            'qBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6ow'.
            'H8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBRe'.
            'qn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; '.
            'top:-22px; width:44px;"></div></div>'.
            '<p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/ayxFjtOiCT/" style=" color:#000; '.
            'font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; '.
            'text-decoration:none; word-wrap:break-word;" target="_blank">My very messy desk has totally awesome toys.</a></p>'.
            '<p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; '.
            'margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">'.
            'A video posted by Christina Warren (@film_girl) on <time style=" font-family:Arial,sans-serif; font-size:14px; '.
            'line-height:17px;" datetime="2013-06-20T19:54:13+00:00">Jun 20, 2013 at 12:54pm PDT</time></p>'.
            '</div></blockquote>'.
            '<p><script async defer src="//platform.instagram.com/en_US/embeds.js"></script></p>'.
            // vimeo embed
            '<p><iframe src="https://player.vimeo.com/video/158647901" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen">'.
            '</iframe></p>'.
            // end vimeo
            '<p><a href="https://vimeo.com/158647901">LEVITATION</a> from <a href="https://vimeo.com/silasveta">SILA SVETA</a> on '.
            '<a href="https://vimeo.com">Vimeo</a>.</p>'.
            '<p>One of the students injured in the Berkeley balcony collapse has thanked another survivor for breaking her fall in '.
            'the accident.</p>'.
            '<p>Clodagh Cogley was one of the students injured after the balcony collapsed in Kittredge Street, Berkeley, yesterday.</p>'.
            '<p>Her brother Darragh Cogley has said that Clodagh is \'awake, upbeat and doing really well\'.</p>'.
            '<p>He says the family can\'t believe how lucky she is to be allright and Clodagh wishes to thank another one of the injured '.
            'students, Jack Halpin, for breaking her fall.</p>'.
            '<blockquote><p>She is awake, somehow upbeat and doing really well.</p>'.
            '<div style="width: 100%; height: 0px; position: relative; padding-bottom: 53.933%;">'.
            '<iframe style="width: 100%; height: 100%; position: absolute;" src="https://streamable.com/e/xvm9" width="300" height="150" '.
            'frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></div>'.
            '<p>We can’t believe how lucky she and the rest of the survivors were and Clodagh wanted to say particular thanks to Jack Halpin '.
            'for grabbing her and breaking her fall.</p></blockquote>'.
            '<p>Clodagh Cogley is the daughter of the former Head of Sport at RTE and current Director of Broadcast at TV3, Niall Cogley.</p>'.
            '<p>Jack Halpin is a footballer with St. Jude\'s GAA club in Tempelogue. He is still seriously injured following yesterday\'s '.
            'tragedy. The others injured are Hannah Waters, Conor Flynn, Niall Murray, Sean Fahey and Aoife Beary.</p>'.
            '<p>Her father spoke to the Independent today, describing it as a \'miracle\' how his daughter was still alive. He said '.
            'she \'seemed chirpy enough given what had happened\' but was unsure whether this was down to the medication she was on.</p>'.
            // vine embed
            '<p><iframe src="https://vine.co/v/iHTTDHz6Z2v/embed/simple" width="600" height="600" frameborder="0"></iframe>'.
            '<script src="https://platform.vine.co/static/scripts/embed.js"></script></p>'.
            // end vine
            '<p><script>// <![CDATA['.
            '(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); '.
            'js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.insertBefore(js, fjs);}'.
            '(document, \'script\', \'facebook-jssdk\'));'.
            '// ]]></script>Quality Football Ireland is an entity that not many football fans will have heard about. It\'s by no means a '.
            'secret but, at the same time, it doesn\'t exactly shout from the rooftops about the fact that it plays a significant part in '.
            'the careers of the some of the world\'s most high profile footballers.</p>'.
            // facebook post
            '<div class="fb-post" data-href="https://www.facebook.com/ballsdotie/posts/10154200625541042" data-width="500">'.
            '<div class="fb-xfbml-parse-ignore">'.
            '<blockquote cite="https://www.facebook.com/ballsdotie/posts/10154200625541042"><p>A fascinating story that\'s about to get an '.
            'awful lot more attention</p>'.
            '<p>Posted by <a href="https://www.facebook.com/ballsdotie/">Balls.ie</a> on <a href="https://www.facebook.com/ballsdotie/posts/'.
            '10154200625541042">Wednesday, April 6, 2016</a></p></blockquote>'.
            '</div></div>'.
            // end facebook post
            '<p>This is a video embed</p>'.
            '<p>&nbsp;</p>'.
            // facebook video
            '<div id="fb-root"></div>'.
            '<p><script>// <![CDATA['.
            '(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); '.
            'js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.insertBefore(js, fjs);}(document'.
            ', \'script\', \'facebook-jssdk\'));'.
            '// ]]></script></p>'.
            '<div class="fb-video" data-allowfullscreen="1" data-href="/ballsdotie/videos/vb.396192711041/10154158359931042/?type=3">'.
            '<div class="fb-xfbml-parse-ignore">'.
            '<blockquote cite="https://www.facebook.com/ballsdotie/videos/10154158359931042/"><p>'.
            '<a href="https://www.facebook.com/ballsdotie/videos/10154158359931042/">Cascarino</a>Pints, in a bar, on Good Friday - Don\'t mind '.
            'if we do.More info: http://bit.ly/1XAMPS7</p>'.
            '<p>Posted by <a href="https://www.facebook.com/ballsdotie/">Balls.ie</a> on Friday, March 25, 2016</p></blockquote>'.
            '</div></div>'.
            // end facebook
            '<p>&nbsp;</p>'.
            // youtube embeds
            '<p><iframe src="https://www.youtube.com/embed/1QfhI1qagBE" width="640" height="360" frameborder="0" allowfullscreen="'.
            'allowfullscreen"></iframe></p>'.
            // end youtube
            '<p>&nbsp;</p>'.
            // twitter embed
            '<blockquote class="twitter-tweet" data-lang="es"><p lang="en" dir="ltr">Watch: Incredible'.
            ' RTÉ Pundit Moment As Didi Hamann Shows Why He&#39;s Not A Youth Coach '.
            '<a href="https://t.co/NZyGb6aThL">https://t.co/NZyGb6aThL</a></p>'.
            '<p>&mdash; Balls.ie (@ballsdotie) <a href="https://twitter.com/ballsdotie/'.
            'status/717822960942186497">6 de abril de 2016</a></p></blockquote>'.
            '<p><script async src="//platform.twitter.com/widgets.js" charset="utf-8">'.
            '</script></p>'.
            // end twitter
            '<p><img class="alignnone size-medium wp-image-297409" src="http://media.balls.ie/uploads/2015/08/26075823/super-meci-pe-emirates-'.
            'arsenal-liverpool-se-joaca-de-la-ora-22-00-echipele-de-start_1_size6-300x135.jpg" alt="super-meci-pe-emirates-arsenal-liverpool-se-'.
            'joaca-de-la-ora-22-00-echipele-de-start_1_size6" width="300" height="135" /></p>'.
            '<p>&nbsp;</p>'.
            '<p><img class="alignnone size-medium wp-image-297414" src="http://media.balls.ie/uploads/2015/09/14155745/seven-ways-social-media-'.
            'has-made-the-gaa-even-more-glorious-300x169.png" alt="seven-ways-social-media-has-made-the-gaa-even-more-glorious" width="300" '.
            'height="169" /></p>'.
            '<blockquote class="imgur-embed-pub" lang="en" data-id="dqgeyfP"><p><a href="//imgur.com/dqgeyfP">View post on imgur.com</a></p>'.
            '</blockquote><p><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script></p>'.
            '<p></p><p>Global footer on Balls</p>'.
            //streamable embed
            '<div style="width: 100%; height: 0px; position: relative; padding-bottom: 53.933%;"><iframe style="width: 100%; height: 100%; '.
            'position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">'.
            '</iframe></div>'.
            //playbuzz embed
            '<p><script src="//cdn.playbuzz.com/widget/feed.js"'.
            ' type="text/javascript"></script></p>'.
            '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
            'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
            'data-recommend="false" data-game-info="false" data-comments="false"></div>'.
            // double wrapping img
            '<h4><a href="http://media.collegetimes.com/uploads/2016/04/08170042/pawisle.jpg" '.
            'rel="attachment wp-att-129641"><img class="aligncenter wp-image-129641" '.
            'src="http://media.collegetimes.com/uploads/2016/04/08170042/pawisle.jpg" '.
            'alt="pawisle" width="600" height="400" /></a></h4>'.
            // nested images
            '<p>First, a Sunderland coach took issue with Brady\'s shove.'.
            '<a href="http://m0.sportsjoe.ie/wp-content/uploads/2016/04/16132449/brady-sunderland-3.1.jpg" '.
            'rel="attachment wp-att-74716"><img class="aligncenter size-full wp-image-74716" '.
            'src="http://m0.sportsjoe.ie/wp-content/uploads/2016/04/16132449/brady-sunderland-3.1.jpg" '.
            'alt="brady sunderland 3.1" width="798" height="398"></a>And&nbsp;Sam Allardyce wasn\'t happy either.'.
            '<a href="http://m0.sportsjoe.ie/wp-content/uploads/2016/04/16132708/Brady-Sunderland-16.png" '.
            'rel="attachment wp-att-74717"><img class="aligncenter wp-image-74717" src="http://m0.sportsjoe.ie/'.
            'wp-content/uploads/2016/04/16132708/Brady-Sunderland-16-1024x596.png" alt="Brady Sunderland 16" '.
            'width="839" height="489"></a>Cue melee.</p>'.
            //getty images
            '<div class="getty embed image" style="background-color: #fff; display: inline-block; font-family:'.
            ' \'Helvetica Neue\',Helvetica,Arial,sans-serif; color: #a7a7a7; font-size: 11px; width: 100%; max-width: 594px;">'.
            '<div style="padding: 0; margin: 0; text-align: left;"><a style="color: #a7a7a7; text-decoration: none; font-weight: '.
            'normal !important; border: none; display: inline-block;" href="http://www.gettyimages.com/detail/512078752" target="_blank">'.
            'Embed from Getty Images</a></div><div style="overflow: hidden; position: relative; height: 0; padding: 67.003367% 0 0 0; '.
            'width: 100%;"><iframe style="display: inline-block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; '.
            'margin: 0;" src="//embed.gettyimages.com/embed/512078752?et=bmD1k8DiS_ht-QgO6UE_Ew&amp;viewMoreLink=off&amp;sig='.
            'BBFi_aBkbHv1-5uJXJ5n5Ffohd99KnxK8-3sweyICNM=" width="594" height="398" frameborder="0" scrolling="no"></iframe></div>'.
            '<p style="margin: 0;"></p></div>';
    }

    /**
     * Expected result
     *
     * @return string Expected Html
     */
    private function getExpectedHtml()
    {
        return '<figure class="op-social"><iframe>'.
            '<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; '.
            'border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; '.
            'max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">'.
            '<div style="padding:8px;">'.
            '<div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;">'.
            '<div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT'.
            '0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81I'.
            'qBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6ow'.
            'H8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBRe'.
            'qn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; '.
            'top:-22px; width:44px;"></div></div>'.
            '<p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/ayxFjtOiCT/" style=" color:#000; '.
            'font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; '.
            'text-decoration:none; word-wrap:break-word;" target="_blank">My very messy desk has totally awesome toys.</a></p>'.
            '<p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; '.
            'margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">'.
            'A video posted by Christina Warren (@film_girl) on <time style=" font-family:Arial,sans-serif; font-size:14px; '.
            'line-height:17px;" datetime="2013-06-20T19:54:13+00:00">Jun 20, 2013 at 12:54pm PDT</time></p>'.
            '</div></blockquote>'.
            '<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>'.
            '</iframe></figure>'.
            // end instagram
            // vimeo embed
            '<figure class="op-social">'.
            '<iframe src="https://player.vimeo.com/video/158647901"   frameborder="0" allowfullscreen="allowfullscreen">'.
            '</iframe></figure>'.
            // end vimeo
            '<p><a href="https://vimeo.com/158647901"><strong>LEVITATION</strong></a> from <a href="https://vimeo.com/silasveta"><strong>SILA SVETA</strong></a> on '.
            '<a href="https://vimeo.com"><strong>Vimeo</strong></a>.</p>'.
            '<p>One of the students injured in the Berkeley balcony collapse has thanked another survivor for breaking her fall in '.
            'the accident.</p>'.
            '<p>Clodagh Cogley was one of the students injured after the balcony collapsed in Kittredge Street, Berkeley, yesterday.</p>'.
            '<p>Her brother Darragh Cogley has said that Clodagh is \'awake, upbeat and doing really well\'.</p>'.
            '<p>He says the family can\'t believe how lucky she is to be allright and Clodagh wishes to thank another one of the injured '.
            'students, Jack Halpin, for breaking her fall.</p>'.
            // blockquote format
            '<br><blockquote><strong><p>She is awake, somehow upbeat and doing really well.</p>'.
            '<figure class="op-social">'.
            '<iframe style="  position: absolute;" src="https://streamable.com/e/xvm9"   '.
            'frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></figure>'.
            '<p>We can&rsquo;t believe how lucky she and the rest of the survivors were and Clodagh wanted to say particular thanks to Jack Halpin '.
            'for grabbing her and breaking her fall.</p></strong></blockquote><br>'.
            // end blockquote
            '<p>Clodagh Cogley is the daughter of the former Head of Sport at RTE and current Director of Broadcast at TV3, Niall Cogley.</p>'.
            '<p>Jack Halpin is a footballer with St. Jude\'s GAA club in Tempelogue. He is still seriously injured following yesterday\'s '.
            'tragedy. The others injured are Hannah Waters, Conor Flynn, Niall Murray, Sean Fahey and Aoife Beary.</p>'.
            '<p>Her father spoke to the Independent today, describing it as a \'miracle\' how his daughter was still alive. He said '.
            'she \'seemed chirpy enough given what had happened\' but was unsure whether this was down to the medication she was on.</p>'.
            // vine embed
            '<figure class="op-social">'.
            '<iframe src="https://vine.co/v/iHTTDHz6Z2v/embed/simple"   frameborder="0"></iframe>'.
            '<script src="https://platform.vine.co/static/scripts/embed.js"></script>'.
            '</figure>'.
            //end vine
            '<p><script>'.
            '(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); '.
            'js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.insertBefore(js, fjs);}'.
            '(document, \'script\', \'facebook-jssdk\'));'.
            '</script>Quality Football Ireland is an entity that not many football fans will have heard about. It\'s by no means a '.
            'secret but, at the same time, it doesn\'t exactly shout from the rooftops about the fact that it plays a significant part in '.
            'the careers of the some of the world\'s most high profile footballers.</p>'.
            // facebook post
            '<figure class="op-social"><iframe>'.
            '<div class="fb-post" data-href="https://www.facebook.com/ballsdotie/posts/10154200625541042" data-width="500">'.
            '<div class="fb-xfbml-parse-ignore">'.
            // blockquote format
            '<br><blockquote cite="https://www.facebook.com/ballsdotie/posts/10154200625541042"><strong><p>A fascinating story that\'s about to get an '.
            'awful lot more attention</p>'.
            '<p>Posted by <a href="https://www.facebook.com/ballsdotie/">Balls.ie</a> on <a href="https://www.facebook.com/ballsdotie/posts/'.
            '10154200625541042">Wednesday, April 6, 2016</a></p></strong></blockquote><br>'.
            // end blockquote
            '</div></div>'.
            '</iframe></figure>'.
            // end facebook post
            '<p>This is a video embed</p>'.
            // facebook video
            '<figure class="op-social"><iframe>'.
            '<div id="fb-root"></div>'.
            '<p><script>'.
            '(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); '.
            'js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3"; fjs.parentNode.insertBefore(js, fjs);}(document'.
            ', \'script\', \'facebook-jssdk\'));'.
            '</script></p>'.
            '<div class="fb-video" data-allowfullscreen="1" data-href="http://www.balls.ie/ballsdotie/videos/vb.396192711041/10154158359931042/?type=3">'.
            '<div class="fb-xfbml-parse-ignore">'.
            // blockquote format
            '<br><blockquote cite="https://www.facebook.com/ballsdotie/videos/10154158359931042/"><strong><p>'.
            '<a href="https://www.facebook.com/ballsdotie/videos/10154158359931042/">Cascarino</a>Pints, in a bar, on Good Friday - Don\'t mind '.
            'if we do.More info: http://bit.ly/1XAMPS7</p>'.
            '<p>Posted by <a href="https://www.facebook.com/ballsdotie/">Balls.ie</a> on Friday, March 25, 2016</p></strong></blockquote><br>'.
            // end blockquote
            '</div></div>'.
            '</iframe></figure>'.
            //end facebook
            //youtube embeds
            '<figure class="op-social">'.
            '<iframe src="https://www.youtube.com/embed/1QfhI1qagBE"   frameborder="0" allowfullscreen="'.
            'allowfullscreen"></iframe>'.
            '</figure>'.
            //end youtube embeds
            // twitter embed
            '<figure class="op-social"><iframe>'.
            '<blockquote class="twitter-tweet" data-lang="es"><p lang="en" dir="ltr">Watch: Incredible'.
            ' RT&Eacute; Pundit Moment As Didi Hamann Shows Why He\'s Not A Youth Coach '.
            '<a href="https://t.co/NZyGb6aThL">https://t.co/NZyGb6aThL</a></p>'.
            '<p>&mdash; Balls.ie (@ballsdotie) <a href="https://twitter.com/ballsdotie/'.
            'status/717822960942186497">6 de abril de 2016</a></p></blockquote>'.
            '<script async src="//platform.twitter.com/widgets.js" charset="utf-8">'.
            '</script></iframe></figure>'.
            // end twitter
            // img embeds
            '<figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments">'.
            '<img class="alignnone size-medium wp-image-297409" src="http://media.balls.ie/uploads/2015/08/26075823/super-meci-pe-emirates-'.
            'arsenal-liverpool-se-joaca-de-la-ora-22-00-echipele-de-start_1_size6-300x135.jpg" alt="super-meci-pe-emirates-arsenal-liverpool-se-'.
            'joaca-de-la-ora-22-00-echipele-de-start_1_size6" width="300" height="135"></figure>'.
            '<figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments">'.
            '<img class="alignnone size-medium wp-image-297414" src="http://media.balls.ie/uploads/2015/09/14155745/seven-ways-social-media-'.
            'has-made-the-gaa-even-more-glorious-300x169.png" alt="seven-ways-social-media-has-made-the-gaa-even-more-glorious" width="300" '.
            'height="169"></figure>'.
            // end img
            // Imgur embed
            '<figure class="op-social"><iframe>'.
            '<blockquote class="imgur-embed-pub" lang="en" data-id="dqgeyfP">'.
            '<a href="http://www.balls.ie/uncategorized/survivor-clodagh-cogley-thanks-injured-gaa-player-for-breaking-her-fall-in-balcony-tragedy'.
            '/297342/imgur.com/dqgeyfP">View post on imgur.com</a></blockquote>'.
            '<script async src="http://www.balls.ie/uncategorized/survivor-clodagh-cogley-thanks-injured-gaa-player-for-breaking-her-fall-in-'.
            'balcony-tragedy/297342/s.imgur.com/min/embed.js" charset="utf-8"></script>'.
            '</iframe></figure>'.
            // end imgur
            '<p>Global footer on Balls</p>'.
            '<figure class="op-social"><iframe style="  '.
            'position: absolute;" src="https://streamable.com/e/xvm9" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">'.
            '</iframe></figure>'.
            // playbuzz
            '<figure class="op-social"><iframe>'.
            '<script src="//cdn.playbuzz.com/widget/feed.js"'.
            ' type="text/javascript"></script>'.
            '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
            'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
            'data-recommend="false" data-game-info="false" data-comments="false"></div>'.
            '</iframe></figure>'.
            '<figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments">'.
            '<img class="aligncenter wp-image-129641" '.
            'src="http://media.collegetimes.com/uploads/2016/04/08170042/pawisle.jpg" '.
            'alt="pawisle" width="600" height="400"></figure>'.
            '<p>First, a Sunderland coach took issue with Brady\'s shove.</p>'.
            '<figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments">'.
            '<img class="aligncenter size-full wp-image-74716" '.
            'src="http://m0.sportsjoe.ie/wp-content/uploads/2016/04/16132449/brady-sunderland-3.1.jpg" '.
            'alt="brady sunderland 3.1" width="798" height="398"></figure>'.
            '<p>And&nbsp;Sam Allardyce wasn\'t happy either.</p>'.
            '<figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments">'.
            '<img class="aligncenter wp-image-74717" src="http://m0.sportsjoe.ie/wp-content/uploads/2016/04/16132708/Brady-Sunderland-16-1024x596.png" '.
            'alt="Brady Sunderland 16" width="839" height="489"></figure><p>Cue melee.</p>'.
            // getty images
            '<figure class="op-social"><div class="getty embed image" style="background-color: #fff; display: inline-block; font-family:'.
            ' \'Helvetica Neue\',Helvetica,Arial,sans-serif; color: #a7a7a7; font-size: 11px; width: 100%; max-width: 594px;">'.
            '<div style="padding: 0; margin: 0; text-align: left;"><a style="color: #a7a7a7; text-decoration: none; font-weight: '.
            'normal !important; border: none; display: inline-block;" href="http://www.gettyimages.com/detail/512078752" target="_blank">'.
            '<strong>Embed from Getty Images</strong></a></div><div style="overflow: hidden; position: relative; height: 0; padding: 67.003367% 0 0 0; '.
            'width: 100%;"><iframe style="display: inline-block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; '.
            'margin: 0;" src="//embed.gettyimages.com/embed/512078752?et=bmD1k8DiS_ht-QgO6UE_Ew&amp;viewMoreLink=off&amp;sig='.
            'BBFi_aBkbHv1-5uJXJ5n5Ffohd99KnxK8-3sweyICNM="   frameborder="0" scrolling="no"></iframe></div>'.
            '</div></figure>';
    }
}
