<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class ImgurEmbedsTest extends TestCase
{

    /**
     * Test Imgur post format embeds function for InstantArticles
     * with Imgur content and relative href url
     *
     * @return void
     */
    public function testFormatImgurForInstantArticleWithRelativeHref()
    {
        $htmlContent = '<p><strong>Is there anything cuter than when animals behave like humans?</strong></p><p>Of course not.</p><p>A gif of a kitten behaving like a regular grateful human being is going viral because, well, the internet loves kittens.</p><p>It was uploaded to <a href="http://imgur.com/gallery/oM9HMez">Imgur</a> two days ago and now has 1.8 million views.</p><p>Theres really not much more to say about this one. Just watch it on loop.</p><p>Happy Monday.</p><blockquote class="imgur-embed-pub" lang="en" data-id="oM9HMez"><p><a href="//imgur.com/oM9HMez">View post on imgur.com</a></p></blockquote><p><script src="//s.imgur.com/min/embed.js" async="" charset="utf-8"></script></p>';
        $postUri = 'life/this-gif-of-a-kitten-clapping-its-paws-for-treats-is-all-you-ever-need-to-see/282774';

        $htmlContent = $this->getFormatterInstance($htmlContent, $postUri)->formatImgur();
        $expected = '<p><strong>Is there anything cuter than when animals behave like humans?</strong></p><p>Of course not.</p><p>A gif of a kitten behaving like a regular grateful human being is going viral because, well, the internet loves kittens.</p><p>It was uploaded to <a href="http://imgur.com/gallery/oM9HMez">Imgur</a> two days ago and now has 1.8 million views.</p><p>Theres really not much more to say about this one. Just watch it on loop.</p><p>Happy Monday.</p><figure class="op-social"><iframe><blockquote class="imgur-embed-pub" lang="en" data-id="oM9HMez"><a href="http://www.balls.ie/life/this-gif-of-a-kitten-clapping-its-paws-for-treats-is-all-you-ever-need-to-see/282774/imgur.com/oM9HMez">View post on imgur.com</a></blockquote><script src="http://www.balls.ie/life/this-gif-of-a-kitten-clapping-its-paws-for-treats-is-all-you-ever-need-to-see/282774/s.imgur.com/min/embed.js" async="" charset="utf-8"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test Imgur post format embeds function for InstantArticles
     * with Imgur content and none relative href url
     *
     * @return void
     */
    public function testFormatImgurForInstantArticleWithNoneRelativeHref()
    {
        $htmlContent = '<p><strong>Is there anything cuter than when animals behave like humans?</strong></p>
            <p>Of course not.</p><p>A gif of a kitten behaving like a regular grateful human being is going viral because, well, the internet loves kittens.</p>
            <p>It was uploaded to <a href="http://imgur.com/gallery/oM9HMez">Imgur</a> two days ago and now has 1.8 million views.</p>
            <p>Theres really not much more to say about this one. Just watch it on loop.</p><p>Happy Monday.</p>
            <blockquote class="imgur-embed-pub" lang="en" data-id="oM9HMez"><p><a href="http://imgur.com/oM9HMez">View post on imgur.com</a></p>
            </blockquote><p><script src="//s.imgur.com/min/embed.js" async="" charset="utf-8"></script></p>';
        $postUri = 'life/this-gif-of-a-kitten-clapping-its-paws-for-treats-is-all-you-ever-need-to-see/282774';
        $htmlContent = $this->getFormatterInstance($htmlContent, $postUri)->formatImgur();

        $expected = '<p><strong>Is there anything cuter than when animals behave like humans?</strong></p>
            <p>Of course not.</p><p>A gif of a kitten behaving like a regular grateful human being is going viral because, well, the internet loves kittens.</p>
            <p>It was uploaded to <a href="http://imgur.com/gallery/oM9HMez">Imgur</a> two days ago and now has 1.8 million views.</p>
            <p>Theres really not much more to say about this one. Just watch it on loop.</p><p>Happy Monday.</p>
            <figure class="op-social"><iframe><blockquote class="imgur-embed-pub" lang="en" data-id="oM9HMez"><a href="http://imgur.com/oM9HMez">View post on imgur.com</a></blockquote><script src="life/this-gif-of-a-kitten-clapping-its-paws-for-treats-is-all-you-ever-need-to-see/282774/s.imgur.com/min/embed.js" async="" charset="utf-8"></script></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test Imgur post format embeds function for InstantArticles
     * with no Imgur content
     *
     * @return void
     */
    public function testFormatImgurForInstantArticleNoneImgurContent()
    {
        $htmlContent = '<a href="https://vimeo.com/hashtag/mufc?src=hash">#mufc</a>
            <a href="https://vimeo.com/hashtag/ChampionsLeague?src=hash">#ChampionsLeague</a>
            <a href="https://vimeo.com/UnitedStandMUFC">@UnitedStandMUFC</a>
            <a href="http://t.co/KpOQjoOJ2w">pic.vimeo.com/KpOQjoOJ2w</a></p>
            <p> Daniel (@MrTeaNBiscuits) <a href="https://vimeo.com/MrTeaNBiscuits/status/649284509486161921">September 30, 2015</a></p>
            <p><script src="//platform.vimeo.com/widgets.js" async="" charset="utf-8"></script></p>
            <p><iframe src="https://player.youtube.com/video/128745697" width="960" height="540" frameborder="0" title="Modestep - Rainbow" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>';
        $postUri = 'life/this-gif-of-a-kitten-clapping-its-paws-for-treats-is-all-you-ever-need-to-see/282774';

        $htmlContent = $this->getFormatterInstance($htmlContent, $postUri)->formatImgur();

        $expected = '<a href="https://vimeo.com/hashtag/mufc?src=hash">#mufc</a>
            <a href="https://vimeo.com/hashtag/ChampionsLeague?src=hash">#ChampionsLeague</a>
            <a href="https://vimeo.com/UnitedStandMUFC">@UnitedStandMUFC</a>
            <a href="http://t.co/KpOQjoOJ2w">pic.vimeo.com/KpOQjoOJ2w</a>
            <p> Daniel (@MrTeaNBiscuits) <a href="https://vimeo.com/MrTeaNBiscuits/status/649284509486161921">September 30, 2015</a></p>
            <p><script src="//platform.vimeo.com/widgets.js" async="" charset="utf-8"></script></p>
            <p><iframe src="https://player.youtube.com/video/128745697" width="960" height="540" frameborder="0" title="Modestep - Rainbow" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
