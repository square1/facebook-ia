<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class RelativeUrlsTest extends TestCase
{

    /**
     * Test complete relative url
     *
     * @return void
     */
    public function testCompleteRelativeUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url with anchord odd content
     *
     * @return void
     */
    public function testCompleteRelativeUrlAnchordOddContent()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            .":http://</a></strong></b></h2>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            .":http://</a></strong></b></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test ignore absolute url
     *
     * @return void
     */
    public function testIgnoreAbsoluteUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="http://www.google.com" target="_blank">
            Testing</a></strong></b></h2>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.google.com" target="_blank">
            Testing</a></strong></b></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test ignore absolute url with anchord odd content
     *
     * @return void
     */
    public function testIgnoreAbsoluteUrlAnchordOddContent()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="http://www.google.com" target="_blank">
            .":http://</a></strong></b></h2>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.google.com" target="_blank">
            .":http://</a></strong></b></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test ignore secure absolute url
     *
     * @return void
     */
    public function testIgnoreSecureAbsoluteUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="https://www.google.com" target="_blank">
            Testing</a></strong></b></h2>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="https://www.google.com" target="_blank">
            Testing</a></strong></b></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url and ignore absolute url
     *
     * @return void
     */
    public function testCompleteRelativeUrlAndIgnoreAbsoluteUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="http://www.google.com">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="http://www.google.com">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url and ignore secure absolute url
     *
     * @return void
     */
    public function testCompleteRelativeUrlAndIgnoreSecureAbsoluteUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="https://www.google.com">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="https://www.google.com">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test ignore absolute url without http
     *
     * @return void
     */
    public function testIgnoreAbsoluteUrlWithoutHTTP()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="www.google.com">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="www.google.com">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test ignore absolute subdomain url without http
     *
     * @return void
     */
    public function testIgnoreAbsoluteSubDomainUrlWithoutHTTP()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="staging.google.com">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="staging.google.com">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url and ignore subdomain absolute url without http
     *
     * @return void
     */
    public function testCompleteRelativeUrlIgnoreAbsoluteSubDomainUrlWithoutHTTP()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="staging.google.com">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
            <a href="staging.google.com">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url and ignore absolute url without http
     *
     * @return void
     */
    public function testCompleteRelativeUrlAndIgnoreAbsoluteUrlWithoutHTTP()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
             <a href="www.google.com">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
             <a href="www.google.com">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test ignore protocol relative url
     *
     * @return void
     */
    public function testIgnoreProtocolRelativeUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="//domain.com/img/logo.png" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="//domain.com/img/logo.png" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url and ignore protocol relative url
     *
     * @return void
     */
    public function testCompleteRelativeUrlAndIgnoreProtocolRelativeUrl()
    {
        $htmlContent = '<h2><b><strong>SEE ALSO:
            <a href="/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
             <a href="//domain.com/img/logo.png">google</a>';
        $expected = '<h2><b><strong>SEE ALSO:
            <a href="http://www.balls.ie/mma/why-the-hell-do-people-insist-on-calling-conor-mcgregor-daddy-on-twitter/327392" target="_blank">
            Why The Hell Do People Insist On Calling Conor McGregor "Daddy" On Twitter?</a></strong></b></h2>
             <a href="//domain.com/img/logo.png">google</a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test complete relative url and ignore protocol relative url using production html
     *
     * @return void
     */
    public function testCompleteRelativeUrlProductionContent()
    {
        $htmlContent = <<<'EOD'
 <article> <header> <h1>England Ready To 'Fast-Track' Leinster Player Into Eddie Jones' Squad</h1> <h3 class="op-kicker"> Rugby </h3> <time class="op-published" datetime="2016-03-29 09:47:09">2016-03-29</time> <time class="op-modified" dateTime="2016-03-29 09:47:09">2016-03-29</time> <address> <a href="http://www.balls.ie/author/pj/44">PJ Browne</a> </address> <figure> <img src="http://media.balls.ie/YToyOntzOjQ6ImRhdGEiO3M6MjYwOiJhOjQ6e3M6MzoidXJsIjtzOjExMDoiaHR0cDovL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tL3N0b3JhZ2UucHVibGlzaGVycGx1cy5pZS9tZWRpYS5iYWxscy5pZS91cGxvYWRzLzIwMTYvMDMvMjkwODU2MjkvMTEyMDA4MC5qcGciO3M6NToid2lkdGgiO2k6NjQ3O3M6NjoiaGVpZ2h0IjtpOjM0MDtzOjc6ImRlZmF1bHQiO3M6NjY6Imh0dHA6Ly9zdGFnaW5nLnd3dy5iYWxscy5pZS5wdWJsaXNoZXJwbHVzLmllL2Fzc2V0cy9pL25vLWltYWdlLnBuZyI7fSI7czo0OiJoYXNoIjtzOjQwOiIyZTc4MDUyZTNiNGFhOWMyNmU1NTVhYWUwZDJmMDc1NTM3ZDc4OGQ4Ijt9/1120080.jpg" /> </figure> <figure class="op-ad"> <iframe src="http://www.balls.ie/dfp/328907/300x250" height="250" width="300"></iframe> </figure> </header> <p>In December, it was announced that Ben Te'o would be leaving Leinster for Premiership side Worcester Warriors.</p><p>The rugby league convert joined Leinster as a project player in 2014 and would have been eligible to play for Ireland by November 2017 had he remained in the country.</p><p>According to the <a href="http://www.dailymail.co.uk/sport/rugbyunion/article-3512954/England-plan-fast-track-rugby-league-convert-Ben-Te-o-union-squad-summer.html">Daily Mail</a>, England have now set their sights on the New Zealand-born centre.</p><p>The Mail say Te'o is being 'fast-tracked' as an option for the 2019 World Cup in Japan.</p><p>The centre has been in excellent form for Leinster this season and does look like he is constantly improving.</p><p>Still, given that Te'o&nbsp;will be 32, nearly 33 by the time the World Cup comes around, you would have to think younger options should be a priority for Eddie Jones.</p><p>Unlike if he had chosen to represent Ireland, Te'o does not have to wait to qualify as an England player as his mother was born and raised there.</p><p><em>Picture credit: Ramsey Cardy / SPORTSFILE</em></p><h2><b><strong>Read:&nbsp;<a href="http://www.balls.ie/rugby/new-zealand-administrator-reveals-intriguing-plan-for-pro12-sides-to-join-super-rugby/328875">New Zealand Administrator Reveals Intriguing Plan For Pro12 Sides To Join Super Rugby</a></strong></b></h2><p>We gave 4 Limerick lads the ultimate viewing experience for the Italy game last weekend thanks to Life Style Sports -<a href="/rugby/watch-munsters-mark-chisholm-got-exactly-what-he-wanted-from-ireland-vs-italy/327433" target="_blank">click here</a>to see how it went.<br><br>Subscribe to Balls.ie podcasts on <a href="https://itunes.apple.com/ie/podcast/balls.ie-podcasts/id1010648356?mt=2">iTunes</a>and<a href="https://soundcloud.com/ballsdotie/">Soundcloud</a></p><p><a href="http://bit.ly/1TEqN2O">Unleash the good times with a great group deal at a night at the dogs.</a><br><br><br><figure data-mode="non-interactive"><img class="wp-image-326330 aligncenter" src="http://s3-eu-west-1.amazonaws.com/storage.publisherplus.ie/media.balls.ie/uploads/2016/03/03154127/Snapchat-copy.jpg" alt="Snapchat copy" width="369" height="369"></figure><br>Got a story or a tip? Email <a href="mailto:thegaffer@balls.ie">thegaffer@balls.ie</a></p> <figure class="op-tracker"> <iframe hidden> <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-7533626-6', 'auto');ga('require', 'displayfeatures');ga('send', 'pageview'); /* Comscore */ var _comscore = _comscore || []; _comscore.push({ c1: "2", c2: "18987979" }); (function() { var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true; s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js"; el.parentNode.insertBefore(s, el); })(); </script> </iframe> </figure> <footer> <ul class="op-related-articles"> <li><a href="http://www.balls.ie/rugby/watch-kieran-marmion-finishes-flowing-move-as-connacht-hit-the-front-against-leinster/328726"></a></li> <li><a href="http://www.balls.ie/rugby/watch-irelands-jack-mcgrath-on-how-he-dealt-with-the-suicide-of-his-brother-in-2010/328095"></a></li> <li><a href="http://www.balls.ie/rugby/mike-ross-expertly-cites-father-ted-to-sum-up-his-contribution-to-stuart-hoggs-try/328052"></a></li> </ul> <small> <a href="http://www.balls.ie">Balls.ie</a> </small> </footer> </article>
EOD;
        $expected = <<<'EOD'
<article> <header> <h1>England Ready To 'Fast-Track' Leinster Player Into Eddie Jones' Squad</h1> <h3 class="op-kicker"> Rugby </h3> <time class="op-published" datetime="2016-03-29 09:47:09">2016-03-29</time> <time class="op-modified" datetime="2016-03-29 09:47:09">2016-03-29</time> <address> <a href="http://www.balls.ie/author/pj/44">PJ Browne</a> </address> <figure> <img src="http://media.balls.ie/YToyOntzOjQ6ImRhdGEiO3M6MjYwOiJhOjQ6e3M6MzoidXJsIjtzOjExMDoiaHR0cDovL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tL3N0b3JhZ2UucHVibGlzaGVycGx1cy5pZS9tZWRpYS5iYWxscy5pZS91cGxvYWRzLzIwMTYvMDMvMjkwODU2MjkvMTEyMDA4MC5qcGciO3M6NToid2lkdGgiO2k6NjQ3O3M6NjoiaGVpZ2h0IjtpOjM0MDtzOjc6ImRlZmF1bHQiO3M6NjY6Imh0dHA6Ly9zdGFnaW5nLnd3dy5iYWxscy5pZS5wdWJsaXNoZXJwbHVzLmllL2Fzc2V0cy9pL25vLWltYWdlLnBuZyI7fSI7czo0OiJoYXNoIjtzOjQwOiIyZTc4MDUyZTNiNGFhOWMyNmU1NTVhYWUwZDJmMDc1NTM3ZDc4OGQ4Ijt9/1120080.jpg"> </figure> <figure class="op-ad"> <iframe src="http://www.balls.ie/dfp/328907/300x250" height="250" width="300"></iframe> </figure> </header> <p>In December, it was announced that Ben Te'o would be leaving Leinster for Premiership side Worcester Warriors.</p><p>The rugby league convert joined Leinster as a project player in 2014 and would have been eligible to play for Ireland by November 2017 had he remained in the country.</p><p>According to the <a href="http://www.dailymail.co.uk/sport/rugbyunion/article-3512954/England-plan-fast-track-rugby-league-convert-Ben-Te-o-union-squad-summer.html">Daily Mail</a>, England have now set their sights on the New Zealand-born centre.</p><p>The Mail say Te'o is being 'fast-tracked' as an option for the 2019 World Cup in Japan.</p><p>The centre has been in excellent form for Leinster this season and does look like he is constantly improving.</p><p>Still, given that Te'o&nbsp;will be 32, nearly 33 by the time the World Cup comes around, you would have to think younger options should be a priority for Eddie Jones.</p><p>Unlike if he had chosen to represent Ireland, Te'o does not have to wait to qualify as an England player as his mother was born and raised there.</p><p><em>Picture credit: Ramsey Cardy / SPORTSFILE</em></p><h2><b><strong>Read:&nbsp;<a href="http://www.balls.ie/rugby/new-zealand-administrator-reveals-intriguing-plan-for-pro12-sides-to-join-super-rugby/328875">New Zealand Administrator Reveals Intriguing Plan For Pro12 Sides To Join Super Rugby</a></strong></b></h2><p>We gave 4 Limerick lads the ultimate viewing experience for the Italy game last weekend thanks to Life Style Sports -<a href="http://www.balls.ie/rugby/watch-munsters-mark-chisholm-got-exactly-what-he-wanted-from-ireland-vs-italy/327433" target="_blank">click here</a>to see how it went.<br><br>Subscribe to Balls.ie podcasts on <a href="https://itunes.apple.com/ie/podcast/balls.ie-podcasts/id1010648356?mt=2">iTunes</a>and<a href="https://soundcloud.com/ballsdotie/">Soundcloud</a></p><p><a href="http://bit.ly/1TEqN2O">Unleash the good times with a great group deal at a night at the dogs.</a><br><br><br><figure data-mode="non-interactive"><img class="wp-image-326330 aligncenter" src="http://s3-eu-west-1.amazonaws.com/storage.publisherplus.ie/media.balls.ie/uploads/2016/03/03154127/Snapchat-copy.jpg" alt="Snapchat copy" width="369" height="369"></figure><br>Got a story or a tip? Email <a href="mailto:thegaffer@balls.ie">thegaffer@balls.ie</a></p> <figure class="op-tracker"> <iframe hidden> <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-7533626-6', 'auto');ga('require', 'displayfeatures');ga('send', 'pageview'); /* Comscore */ var _comscore = _comscore || []; _comscore.push({ c1: "2", c2: "18987979" }); (function() { var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true; s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js"; el.parentNode.insertBefore(s, el); })(); </script> </iframe> </figure> <footer> <ul class="op-related-articles"> <li><a href="http://www.balls.ie/rugby/watch-kieran-marmion-finishes-flowing-move-as-connacht-hit-the-front-against-leinster/328726"></a></li> <li><a href="http://www.balls.ie/rugby/watch-irelands-jack-mcgrath-on-how-he-dealt-with-the-suicide-of-his-brother-in-2010/328095"></a></li> <li><a href="http://www.balls.ie/rugby/mike-ross-expertly-cites-father-ted-to-sum-up-his-contribution-to-stuart-hoggs-try/328052"></a></li> </ul> <small> <a href="http://www.balls.ie">Balls.ie</a> </small> </footer> </article>
EOD;
        $htmlContent = $this->getFormatterInstance($htmlContent)->completeRelativeUrls();
        $this->assertEquals($expected, $htmlContent);
    }
}