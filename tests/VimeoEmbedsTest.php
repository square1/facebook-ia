<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class VimeoEmbedsTest extends TestCase
{

    /**
     * Test the vimeo post embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatVimeo()
    {
        $htmlContent = '<a href="https://twitter.com/hashtag/mufc?src=hash">#mufc</a>
            <a href="https://twitter.com/hashtag/ChampionsLeague?src=hash">#ChampionsLeague</a>
            <a href="https://twitter.com/UnitedStandMUFC">@UnitedStandMUFC</a>
            <a href="http://t.co/KpOQjoOJ2w">pic.twitter.com/KpOQjoOJ2w</a></p>
            <p> Daniel (@MrTeaNBiscuits) <a href="https://twitter.com/MrTeaNBiscuits/status/649284509486161921">September 30, 2015</a></p>
            <p><script src="//platform.twitter.com/widgets.js" async="" charset="utf-8"></script></p>
            <p><iframe src="https://player.vimeo.com/video/128745697" width="960" height="540" frameborder="0" title="Modestep - Rainbow" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatVimeo();

        $expected = '<a href="https://twitter.com/hashtag/mufc?src=hash">#mufc</a>
            <a href="https://twitter.com/hashtag/ChampionsLeague?src=hash">#ChampionsLeague</a>
            <a href="https://twitter.com/UnitedStandMUFC">@UnitedStandMUFC</a>
            <a href="http://t.co/KpOQjoOJ2w">pic.twitter.com/KpOQjoOJ2w</a>
            <p> Daniel (@MrTeaNBiscuits) <a href="https://twitter.com/MrTeaNBiscuits/status/649284509486161921">September 30, 2015</a></p>
            <p><script src="//platform.twitter.com/widgets.js" async="" charset="utf-8"></script></p>
            <p><figure class="op-social"><iframe src="https://player.vimeo.com/video/128745697" width="960" height="540" frameborder="0" title="Modestep - Rainbow" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></figure></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the vimeo post format embeds function for InstantArticles
     * without or with similar vimeo post info.
     *
     * @return void
     */
    public function testFormatVimeoForInstantArticleNoneFacebookPost()
    {
        $htmlContent = '<a href="https://vimeo.com/hashtag/mufc?src=hash">#mufc</a>
            <a href="https://vimeo.com/hashtag/ChampionsLeague?src=hash">#ChampionsLeague</a>
            <a href="https://vimeo.com/UnitedStandMUFC">@UnitedStandMUFC</a>
            <a href="http://t.co/KpOQjoOJ2w">pic.vimeo.com/KpOQjoOJ2w</a></p>
            <p> Daniel (@MrTeaNBiscuits) <a href="https://vimeo.com/MrTeaNBiscuits/status/649284509486161921">September 30, 2015</a></p>
            <p><script src="//platform.vimeo.com/widgets.js" async="" charset="utf-8"></script></p>
            <p><iframe src="https://player.youtube.com/video/128745697" width="960" height="540" frameborder="0" title="Modestep - Rainbow" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatVimeo();

        $expected = '<a href="https://vimeo.com/hashtag/mufc?src=hash">#mufc</a>
            <a href="https://vimeo.com/hashtag/ChampionsLeague?src=hash">#ChampionsLeague</a>
            <a href="https://vimeo.com/UnitedStandMUFC">@UnitedStandMUFC</a>
            <a href="http://t.co/KpOQjoOJ2w">pic.vimeo.com/KpOQjoOJ2w</a>
            <p> Daniel (@MrTeaNBiscuits) <a href="https://vimeo.com/MrTeaNBiscuits/status/649284509486161921">September 30, 2015</a></p>
            <p><script src="//platform.vimeo.com/widgets.js" async="" charset="utf-8"></script></p>
            <p><iframe src="https://player.youtube.com/video/128745697" width="960" height="540" frameborder="0" title="Modestep - Rainbow" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
