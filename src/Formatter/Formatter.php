<?php

namespace Square1\Formatter;

/**
* Facebook Instant Article Formatter class
*/
class Formatter
{
    protected $htmlContent;
    protected $postUri;

    /**
     * Formatter class constructor
     *
     * @param string $this->htmlContent Html content
     * @param string $postUri     Post seo url
     */
    public function __construct($htmlContent, $postUri = null)
    {

        $this->isValidContent($htmlContent);
        $this->isValidString($postUri);
        $this->htmlContent = $htmlContent;
        $this->htmlContent = $this->setWellFormattedHTMLContent();
        $this->postUri = $postUri;
    }

    /**
     * Check if the given html content is valid
     *
     * @param  string  $this->htmlContent Html content string
     *
     * @return mixed
     */
    private function isValidContent($htmlContent)
    {
        if (empty($htmlContent)) {
            throw new \Exception('Html Content can\'t be empty or null.');
        }
        if (!is_string($htmlContent)) {
            throw new \Exception('Html Content must be string.');
        }
        return true;
    }

    /**
     * Check is the given url param is valid
     *
     * @return mixed
     */
    private function isValidString($postUri)
    {
        if (!empty($postUri) && !is_string($postUri)) {
            throw new \Exception('Only strings are allowed.');
        }
        return true;
    }

    /**
     * Run all the formatting function
     *
     * @return string $htmlContent Content formatted
     */
    public function getFormattedContent()
    {
        $this->setWellFormattedHTMLContent();
        $this->formatYouTube();
        $this->formatTwitter();
        $this->formatVine();
        $this->formatInstagram();
        $this->formatFacebookVideo();
        $this->formatFacebookPost();
        $this->formatVimeo();
        $this->formatImgur();
        $this->formatGetttyImages();
        $this->formatStreamable();
        $this->formatRssPost();
        $this->removeUnsupportedHeadingTypes();
        $this->formatHeadings();
        $this->removeHtmlAroundImages();
        $this->formatPollDaddy();
        $this->formatPlayBuzz();
        $this->removeEmptyATags();
        $this->removeEmptyHtmlTags();
        $this->wrapImgWithFigureTag();
        $this->completeRelativeUrls();
        $this->removeIframeWrappingTags();
        $this->removeFigureWrappingTags();
        $this->removeEmptyPTags();
        $this->formatOoyala();
        $this->formatSoundcloud();
        $this->formatContentBlockquotes();
        $this->formatContentAnchords();
        $this->removeWidthAndHeightFromEmbeds();
        $this->removeWidthAndHeightFromIframes();
        $this->topLevelImageEmbeds();
        return $this->htmlContent;
    }

    /**
     * Get the current htmlContent as a DOMDocument object
     *
     * @return DOMDocument $dom DOMDocument instance of htmlContent
     */
    private function domHtmlContent()
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
        $dom->loadHTML(mb_convert_encoding($this->htmlContent, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NODEFDTD);
        return $dom;
    }

    /**
     * DOM extension saveHTML method returns a well formatted html content, meaning that unclosed tags will now be closed
     * This method should be implemented before any other IA formatting function.
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function setWellFormattedHTMLContent()
    {
        $dom = $this->domHtmlContent();
        $this->htmlContent = $dom->saveHTML();
        $this->htmlContent = $this->stripHtmlHeaders($this->htmlContent);
        return $this->htmlContent;
    }

    /**
     * Remove Doctype, html and body tags after DOMDocument save()
     *
     * @param  string $string DOMDocument saved string
     *
     * @return string $this->htmlContnet Content Formatted
     */
    private function stripHtmlHeaders($string = '')
    {
        return trim(preg_replace(
            '/^<!DOCTYPE.+?>/',
            '',
            str_replace(
                array('<html>', '</html>', '<body>', '</body>'),
                array('', '', '', ''),
                $string
            )
        ));
    }

    /**
     * Format post content to support polldaddy embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatPollDaddy()
    {
        // polldaddy pattern in the article body
        $pattern = "/<script .*?src=(\"|')http:\/\/static\.polldaddy.*?(\"|').*?<\/script>.*?\n?.*?<noscript>.*?<\/noscript>/";
        if (preg_match($pattern, $this->htmlContent, $matches)) {
            $replacement = str_replace('</script></p>', '</script>', $matches[0]);
            $this->htmlContent = preg_replace($pattern, '<figure class="op-social"><iframe>'.$replacement.'</iframe></figure>', $this->htmlContent);
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support soundcloud embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatSoundcloud()
    {
        // Soundcloud pattern in the article body
        $pattern = '/<iframe([^>]*)src="(([^.]*).soundcloud([^"]*))"([^>]*)><\/iframe>/';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe$1src="$2"$5></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support youtube embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatYouTube()
    {
        // Youtube pattern in the article body
        $pattern = '/<iframe([^>]*)src="(([^.]*).youtu([^"]*))"([^>]*)><\/iframe>/';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe$1src="$2"$5></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support youtube embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatTwitter()
    {
        $pattern = '/(<blockquote class="twitter(?:[^>]*)>((?:.|\s*)*?)<\/blockquote>(\s*))(?:\s*\\n\s*)?((?:\s*)?(?:<[^>]*>(?:\s*)*)?((<script.+?|\s*?)(<\/script>))(?:\s*<\/[^>]*>\s*)*)?/im';

        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe>$1$5</iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support vine embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatVine()
    {
        // Vine pattern in the article body
        $pattern = '/<p><iframe([^>]*)src="(([^.]*).vine.co([^"]*))"([^>]*)>(.*?)<\/script><\/p>/s';

        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe$1src="$2"$5>$6</script></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support instagram embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatInstagram()
    {
        // Instagram pattern in the article body
        $pattern = '/<blockquote class="instagram-media"([\s\S]*?)<\/blockquote>([\s\S]*?)<p><script([\s\S]*?)<\/script><\/p>/s';

        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe><blockquote class="instagram-media"$1</blockquote>$2<script$3</script></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support facebook video embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatFacebookVideo()
    {
        // Facebook video pattern in the article body
        // Fb videos can come with a div class fb-post or fb-video keep an eye on it
        // if you're digging into this embed
        $pattern = '/<div([^>]*?)id="fb-root"((\s|\n|\t|\r|.)+?)<\/div>((\s|\n)*?)<\/div>/s';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe><div id="fb-root"$2</div></div></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support facebook post embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatFacebookPost()
    {
        $pattern = '/<div([^>]*?)class="fb-post"((\s|\n|\t|\r|.)+?)(<div[^>]*?>(.*?)<\/div>)?((\s|\n)*?)<\/div>/s';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe><div class="fb-post"$2$4$6</div></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support vimeo embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatVimeo()
    {
        // Vimeo pattern in the article body
        $pattern = '/<iframe([^>]*)src="(([^.]*).vimeo.com([^"]*))"([^>]*)><\/iframe>/';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe$1src="$2"$5></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support Imgur Scripts
     * This will remove all the empty p tags
     * and will add the uri base to relative ones.
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatImgur()
    {
        // Imgur pattern in the article body
        $relativePattern = '/(<blockquote[^>]*imgur-embed-pub[^>]*>)(?:<p>)?(<a[^>]*href=")\/\/([^"]*)("[^>]*>.+?<\/a>)(?:<\/p>)?\s*?(<\/blockquote>)(?:<p>)?(<script[^>]*?src=")\/\/([^"]*)("[^>]*>\s*?<\/script>)(?:<\/p>)?/im';
        $nonRelativePattern = '/(<blockquote[^>]*imgur-embed-pub[^>]*>)(?:<p>)?(<a[^>]*href="http:\/\/)([^"]*)("[^>]*>.+?<\/a>)(?:<\/p>)?\s*?(<\/blockquote>)(?:<p>)?(<script[^>]*?src=")\/\/([^"]*)("[^>]*>\s*?<\/script>)(?:<\/p>)?/im';

        // looking for no-relative url
        if (preg_match($nonRelativePattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $nonRelativePattern,
                '<figure class="op-social"><iframe>$1$2$3$4$5$6'.$this->postUri.'/$7$8</iframe></figure>',
                $this->htmlContent
            );
        }

        // looking for relative url
        if (preg_match($relativePattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $relativePattern,
                '<figure class="op-social"><iframe>$1$2'.getenv('SITE_URL').'/'.$this->postUri.'/$3$4$5$6'.getenv('SITE_URL').'/'.$this->postUri.'/$7$8</iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Format post content to support Ooyala embeds
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatOoyala()
    {
        // Ooyala pattern in the article body
        $pattern = '/<script([^>]*)src="(([^.]*).ooyala([^"]*))"([^>]*)><\/script>/';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe><script$1src="$2"$5></script></iframe></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Replace all heading tags from h3 to h6 for h2 tags
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeUnsupportedHeadingTypes()
    {
        if (!preg_match_all("/(<h|<\/h)[3-6]{1}/", $this->htmlContent, $matched_headings)) {
            return $this->htmlContent;
        }
        foreach ($matched_headings[0] as $heading) {
            // when heading is lenght less than 4 is an opening tag if not is a closing tag
            $replace = strlen($heading) < 4 ? '<h2' : '</h2';
            $this->htmlContent = str_replace($heading, $replace, $this->htmlContent);
        }
        return $this->htmlContent;
    }

    /**
     * Remove p and a tags wrapping img tags
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeHtmlAroundImages()
    {
        if (empty($this->htmlContent)) {
            return $this->htmlContent;
        }
        $pattern = '/(<a[^>]*>)<img ([^>]*)>(<\/a>)/';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<img $2>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Remove p and a tags with empty content (<p><a href="someting"><\><\p>)
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeEmptyATags()
    {
        if (empty($this->htmlContent)) {
            return $this->htmlContent;
        }
        $pattern = '/<p><a(.*?)href="([^>]*)><\/a><\/p>/';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Removes p tags that wraps around iframe tags
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeIframeWrappingTags()
    {
        $wrapping_tags = 'p|a|strong|span|h1|h2|h3|h4|h5|h6';
        $dom = $this->domHtmlContent();

        foreach ($dom->getElementsByTagName('iframe') as $element) {
            if (!empty($element->parentNode->textContent)) {
                //tag have text
                continue;
            }
            if (strpos($wrapping_tags, $element->parentNode->nodeName) !== false) {
                $unwrapped_html = '';
                $wrapped_html = $element->parentNode;
                foreach ($wrapped_html->childNodes as $child) {
                    $unwrapped_html = $unwrapped_html.$dom->saveHTML($child);
                }
                $this->htmlContent = str_replace($dom->saveHTML($wrapped_html), $unwrapped_html, $this->htmlContent);
            }
        }

        return $this->htmlContent;
    }

    /**
     * Removes p tags that wraps around figure tags
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeFigureWrappingTags()
    {
        $pattern = '/<(b|p|a|strong|span|h[1-6])[^>]*>\s*?(<figure[^>]*>(.*?)<\/figure>)\s*?<\/\1[^>]*>/im';
        while (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace($pattern, '$2', $this->htmlContent);
        }
        return $this->htmlContent;
    }

    /**
     * Removes empty p tags
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeEmptyPTags()
    {
        $dom = $this->domHtmlContent();
        $content = $dom->saveHTML();
        $content = $this->stripHtmlHeaders($content);
        foreach ($dom->getElementsByTagName('p') as $element) {
            if (empty($element->textContent)) {
                $elementAsText = trim($dom->saveHTML($element));
                $elementFormatted = trim(preg_replace('/<p[^>]*>(.*?)<\/p[^>]*>/im', '$1', $elementAsText));
                $content = str_replace($elementAsText, $elementFormatted, $content);
            }
            $this->htmlContent = $content;
        }


        return $this->htmlContent;
    }

    /**
     * Remove all empty tags from content
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeEmptyHtmlTags()
    {
        $pattern = '/<(?!iframe|div|figure|script[^>]|img)(\w+)(?![^>]*id="fb-root")[^>]*>(?:>[\p{Z}\p{C}]|<br\b[^>]*>|&(?:(?:nb|thin|zwnb|e[nm])sp|zwnj|#xfeff|#xa0|#160|#65279);|(?R))*(?:\s*)?<\/\1>/imsU';
        $text = $this->htmlContent;
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace($pattern, '$2', $text);
        }

        return $this->htmlContent;
    }

    /**
     * It wraps html elements inside h2 tags with b tags. This function must be implemented after removeUnsupportedHeadingTypes
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatHeadings()
    {
        if (!preg_match("/<h2([^>]*)>(.*?)<\/h2>/s", $this->htmlContent)) {
            return $this->htmlContent;
        }
        $this->htmlContent = preg_replace("/<h2(\s*.*?)>(.*?)<\/h2>/s", "<h2$1><b>$2</b></h2>", $this->htmlContent);
        return $this->htmlContent;
    }

    /**
     * Remove width and height from iframe tags
     *
     * @param array $post Post data
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeWidthAndHeightFromIframes()
    {
        // Youtube embeds doesn't need width and height attributes
        $patternWidth  = '/(<iframe[^>]*)(width((=")|:)[0-9]*(%|px|)("|;))([^>]*>(\s*?)(<\/iframe[^>]*>))/is';
        $patternHeight = '/(<iframe[^>]*)(height((=")|:)[0-9]*(%|px|)("|;))([^>]*>(\s*?)(<\/iframe[^>]*>))/is';
        $patterns = ['height' => $patternHeight, 'width' => $patternWidth];
        foreach ($patterns as $key => $pattern) {
            if (preg_match($pattern, $this->htmlContent)) {
                $this->htmlContent = preg_replace(
                    $pattern,
                    '$1$7',
                    $this->htmlContent
                );
            }
        }

        return $this->htmlContent;
    }

    /**
     * Remove width and height inside op-social embeds
     *
     * @param  array $post Post data
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function removeWidthAndHeightFromEmbeds()
    {
        $dom = $this->domHtmlContent();
        $finder = new \DomXPath($dom);
        $classname="op-social";
        $nodes = $finder->query("//*[contains(@class, '$classname')]");
        $pattern = '/((height|width)(:|=")\s?[0-9]*(%|px)?("|;)?)/im';
        foreach ($nodes as $key => $node) {
            $nodeAsHtml = $dom->saveHTML($node);
            if (preg_match($pattern, $nodeAsHtml)) {
                $nodeFormatted = preg_replace($pattern, '', $nodeAsHtml);
                $this->htmlContent = str_replace($nodeAsHtml, $nodeFormatted, $this->htmlContent);
            }
        }

        return $this->htmlContent;
    }

    /**
     * Wrap all the img with a figure tag.
     * FB IA brings an interactive feature by default
     * but requires High resolution to work it is needed
     * to add a attr data-mode non-interactive and
     * data-feedbak fbLlikes,fbLcomments to the figure
     * to avoid this failing and launching an error
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function wrapImgWithFigureTag()
    {
        $pattern = '/<img([^>]*)>/s';

        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure data-mode="non-interactive" data-feedback="fb:likes,fb:comments"><img$1></figure>',
                $this->htmlContent
            );
        }

        return $this->htmlContent;
    }

    /**
     * Complete relative urls
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function completeRelativeUrls()
    {
        $pattern = "/href=(?:\'|\")(\/[^\/]+.*?)(?:\'|\")/";

        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                'href="'.getenv('SITE_URL').'/$1"',
                $this->htmlContent
            );
            $this->htmlContent = str_replace(getenv('SITE_URL').'//', getenv('SITE_URL').'/', $this->htmlContent);
        }

        return $this->htmlContent;
    }

    /**
     * Formatting content blockquotes adding spaces and boldtags to the blockquote content
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatContentBlockquotes()
    {
        $forbidden_blockquotes = ['class="imgur-embed-pub"', 'class="instagram-media', 'class="twitter'];
        preg_match_all("/(?<!<iframe>)(<blockquote\s*(.*?)>)(.*?)(<\/blockquote>)/s", $this->htmlContent, $matched_blockquotes);
        for ($i=0; $i<count($matched_blockquotes[0]); $i++) {
            foreach ($forbidden_blockquotes as $forbidden_blockquote) {
                if (strpos($matched_blockquotes[2][$i], $forbidden_blockquote) !== false) {
                    // if the blockqoute has any of this classes we break the first loop
                    continue 2;
                }
            }
            $unspaced_blockquote = $matched_blockquotes[0][$i];
            $spaced_blockquote = '<br>'.$matched_blockquotes[1][$i].'<strong>'.$matched_blockquotes[3][$i].'</strong>'.$matched_blockquotes[4][$i].'<br>';
            $this->htmlContent = str_replace($unspaced_blockquote, $spaced_blockquote, $this->htmlContent);
        }

        return $this->htmlContent;
    }

    /**
     * Formatting content anchord links adding spaces and boldtags to the anchord links content
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatContentAnchords()
    {
        $forbidden_blockquotes = ['class="imgur-embed-pub"', 'class="instagram-media', 'class="twitter'];
        $post_content = $this->htmlContent;
        preg_match_all("/<blockquote\s*(.*?)>(.*?)<\/blockquote>/s", $post_content, $matched_blockquotes);
        for ($i=0; $i<count($matched_blockquotes[0]); $i++) {
            foreach ($forbidden_blockquotes as $forbidden_blockquote) {
                if (strpos($matched_blockquotes[1][$i], $forbidden_blockquote) !== false) {
                    // this var is a copy of the content, in this copy all a tags inside embeds are remove
                    // this way we can find the other tags with a more simple regex
                    $ancords_deleted = preg_replace("/<a\s*.*?>.*?<\/a>/s", "", $matched_blockquotes[2][$i]);
                    $post_content = str_replace($matched_blockquotes[2][$i], $ancords_deleted, $post_content);
                }
            }
        }
        // removing anchords inside strong tags
        preg_match_all("/<strong\s*.*?>(.*?)<\/strong>/s", $post_content, $matched_boldtags);
        for ($i=0; $i<count($matched_boldtags[0]); $i++) {
            if (strpos($matched_boldtags[1][$i], '<a') !== false) {
                $ancords_deleted = preg_replace("/<a\s*.*?>.*?<\/a>/s", "", $matched_boldtags[1][$i]);
                $post_content = str_replace($matched_boldtags[1][$i], $ancords_deleted, $post_content);
            }
        }
        // all the ancords inside embeds and strong tags were removed form the $post_content var
        preg_match_all("/(<a\s*.*?>)(.*?)(<\/a>)/s", $post_content, $matched_anchords);
        for ($i=0; $i<count($matched_anchords[0]); $i++) {
            $old_tag = $matched_anchords[0][$i];
            $new_tag = $matched_anchords[1][$i].'<strong>'.$matched_anchords[2][$i].'</strong>'.$matched_anchords[3][$i];
            $this->htmlContent = str_replace($old_tag, $new_tag, $this->htmlContent);
        }
        return $this->htmlContent;
    }

    /**
     * Format Streamable embeds for Facebook IA
     *
     * @param  array $post Post data
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatStreamable()
    {
        $pattern = '/(?:<div[^>]*?>)(<iframe[^>]*src="[^"]*streamable.com[^"]*"[^>]*>(\s)*?<\/iframe[^>]*>(\s)*?)(?:<\/div[^>]*>)/im';
        $replacement = '<figure class="op-social">$1</figure>';
        preg_match_all($pattern, $this->htmlContent, $matches);
        if (!empty($matches)) {
            $this->htmlContent = preg_replace($pattern, $replacement, $this->htmlContent);
        }
        return $this->htmlContent;
    }

    /**
     * PlayBuzz embeds support for Facebook IA
     *
     * @param  array $post Post data
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatPlayBuzz()
    {
        $pattern = '/(?:<p>)?\n?(<script[^>]*playbuzz\.com[^>]*><\/script>)\n?(?:<\/p>)?\s*?(<div[^>]*><\/div>)/im';
        if (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace(
                $pattern,
                '<figure class="op-social"><iframe>$1$2</iframe></figure>',
                $this->htmlContent
            );
        }
        return $this->htmlContent;
    }

    /**
     * Custom formatting for posts destined to appear on rss feeds
     *
     * @param array $post Post array
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function formatRssPost()
    {

        // Remove CDATA. When this appears in side a feed which is already wrapped in CDATA tags, the
        // parser goes crazy and rejects it. Balls have a couple of cdata formats, depending on the source
        // which they have pasted in, so remove both variants
        $this->htmlContent = str_replace(
            '// <![CDATA[',
            '',
            str_replace('// ]]>', '', $this->htmlContent)
        );
        $this->htmlContent = preg_replace('#/\* <!\[CDATA\[ \*/#', '', $this->htmlContent);
        $this->htmlContent = preg_replace('#/\* \]\]> \*/#', '', $this->htmlContent);
        // Remove dodgy non-utf8 chars. Not perfect, as we may lose some accented chars, but iconv gives no joy
        $this->htmlContent = preg_replace('/[^(\x20-\x7F)]*/', '', $this->htmlContent);

        return $this->htmlContent;
    }

    /**
     * Facebook ignores image embeds when this are not at the top level
     * so we need to move them to make it visible.
     *
     * @return string $this->htmlContnet Content Formatted
     */
    public function topLevelImageEmbeds()
    {
        $pattern = '/(<(\w)[^>]*>[^<]*?)(<figure[^>]*>[^<]*?(?=<img[^>]*?>).*?<\/figure[^>]*>)/im';
        while (preg_match($pattern, $this->htmlContent)) {
            $this->htmlContent =  preg_replace($pattern, '$1</$2>$3<$2>', $this->htmlContent);
            // in some cases this could return and empty tag
            $this->removeEmptyHtmlTags();
        }
        return $this->htmlContent;
    }

    /**
     * Support for GettyImage embeds on instant article feed
     *
     * @param  array $post Post data
     *
     * @return array $post Post data with formatted content
     */
    public function formatGetttyImages()
    {
        $embedPattern = '/(<div[^>]*class=\"getty embed image\"[^>]*>(.*?)<\/iframe><\/div>(.*?)<\/div>)/im';

        if (preg_match($embedPattern, $this->htmlContent)) {
            $this->htmlContent = preg_replace($embedPattern, '<figure class="op-social">$1</figure>', $this->htmlContent);
        }

        return $this->htmlContent;
    }
}
