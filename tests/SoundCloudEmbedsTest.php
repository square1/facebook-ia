<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class SoundCloudEmbedTest extends TestCase
{

    /**
     * Test the formatting of soundcloud embeds
     *
     * @return void
     */
    public function testFormatSoundcloud()
    {
        $htmlContent = '<iframe width="100%" height="166" scrolling="no" frameborder="no" '
            . 'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/196567484'
            . '&amp;color=4d941e"></iframe>'
            . '<iframe width="100%" height="166" scrolling="no" frameborder="no" '
            . 'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1234567'
            . '&amp;color=4d941e"></iframe>'
            . 'Some non soundcloud text'
            . '<iframe width="100%" height="166" scrolling="no" frameborder="no" '
            . 'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/9876543'
            . '&amp;color=4d941e"></iframe>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatSoundcloud();
        $expected = '<figure class="op-social"><iframe width="100%" height="166" scrolling="no" frameborder="no" '
            . 'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/196567484'
            . '&amp;color=4d941e"></iframe></figure>'
            . '<figure class="op-social"><iframe width="100%" height="166" scrolling="no" frameborder="no" '
            . 'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1234567'
            . '&amp;color=4d941e"></iframe></figure>'
            . 'Some non soundcloud text'
            . '<figure class="op-social"><iframe width="100%" height="166" scrolling="no" frameborder="no" '
            . 'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/9876543'
            . '&amp;color=4d941e"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
