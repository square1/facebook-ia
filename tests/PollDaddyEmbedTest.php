<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class PollDaddyEmbedTest extends TestCase
{
    /**
     * Test the formatting of the post content polldaddy embeds
     *
     * @return void
     */
    public function testFormatPollDaddyEmbeds()
    {
        $htmlContent = '<p>Test Poll</p><p><script src="http://static.polldaddy.com/p/9339811.js" type="text/javascript" charset="utf-8"></script></p><noscript><a href="http://polldaddy.com/poll/9339811/">Do You Think Kids Should Be Invited To Weddings?</a></noscript></p>';
        $expected = '<p>Test Poll</p><p><figure class="op-social"><iframe><script src="http://static.polldaddy.com/p/9339811.js" type="text/javascript" charset="utf-8"></script><noscript><a href="http://polldaddy.com/poll/9339811/">Do You Think Kids Should Be Invited To Weddings?</a></noscript></iframe></figure>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatPollDaddy();
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the formatting of the post content without polldaddy embeds
     *
     * @return void
     */
    public function testFormatNoPollDaddyEmbeds()
    {
        $htmlContent = '<p>Test Poll</p><p><figure class="op-social"><iframe><script type="text/javascript" charset="utf-8"></script><noscript><a href="http://polldaddy.com/poll/9339811/">Do You Think Kids Should Be Invited To Weddings?</a></noscript></iframe></figure></p>';
        $expected = '<p>Test Poll</p><p><figure class="op-social"><iframe><script type="text/javascript" charset="utf-8"></script><noscript><a href="http://polldaddy.com/poll/9339811/">Do You Think Kids Should Be Invited To Weddings?</a></noscript></iframe></figure></p>';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatPollDaddy();
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the formatting of the post content without polldaddy embeds using plain html tags
     *
     * @return void
     */
    public function testFormatNoPollDaddyEmbedsPlainHtml()
    {
        $htmlContent = '<p>Test Poll</p><p>Lorem Ipsum</p><img src="pablic/img.png" >';
        $expected = '<p>Test Poll</p><p>Lorem Ipsum</p><img src="pablic/img.png">';
        $htmlContent = $this->getFormatterInstance($htmlContent)->formatPollDaddy();
        $this->assertEquals($expected, $htmlContent);
    }
}

