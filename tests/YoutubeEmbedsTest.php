<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class YoutubeEmbedsTest extends TestCase
{

    /**
     * Test the youtube embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatYouTube()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
            <p>He chose the Stark sigil as a tribute to... A very well know character.</p>
            <p><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatYouTube();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><figure class="op-social"><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></figure></p>
            <p>He chose the Stark sigil as a tribute to... A very well know character.</p>
            <p><figure class="op-social"><iframe width="480" height="270" src="https://www.youtube.com/embed/2fHlSkFsjVk?feature=oembed" frameborder="0" allowfullscreen></iframe></figure></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the youtube format embeds function for InstantArticles
     * without or with similar youtube info.
     *
     * @return void
     */
    public function testFormatYouTubeForInstantArticleNoneYoutubeContent()
    {
        $htmlContent = '<p src="youtube"><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">I vote Tory <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">
            #BadSexIn3Words</a></p><p> youtube (@PSyvret) <a href="https://twitter.com/PSyvret/status/707138248603140096">March 8, 2016</a></p></blockquote>
            <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p><blockquote class="twitter-tweet">
            <p lang="en" dir="ltr">"Whens International Men-" <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a></p>
            <p> Liam Dryden (@LiamDrydenEtc) <a href="https://twitter.com/LiamDrydenEtc/status/707150977250566144">March 8, 2016</a></p></blockquote>
            <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Luke and Yoda <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p></blockquote>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatYouTube();

        $expected = '<p src="youtube"><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">I vote Tory <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">
            #BadSexIn3Words</a></p><p> youtube (@PSyvret) <a href="https://twitter.com/PSyvret/status/707138248603140096">March 8, 2016</a></p></blockquote>
            <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p><blockquote class="twitter-tweet">
            <p lang="en" dir="ltr">"Whens International Men-" <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a></p>
            <p> Liam Dryden (@LiamDrydenEtc) <a href="https://twitter.com/LiamDrydenEtc/status/707150977250566144">March 8, 2016</a></p></blockquote>
            <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Luke and Yoda <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p></blockquote>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
