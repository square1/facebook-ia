<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class EmptyAtagsTest extends TestCase
{

    /**
     * Test removing empty p tags with empty a tags inside
     * This test will check the function with empty a tag.
     * and multiple imgs
     *
     * @return void
     */
    public function testRemovingEmptyATags()
    {
        $htmlContent = '<p><a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank"></a></p><p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p>';

        $expected = '<p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyATags();


        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test removing empty p tags with empty a tags inside
     * This test will check the function with none empty a tag.
     * and multiple imgs
     *
     * @return void
     */
    public function testRemovingEmptyATagsWithNoneEmptyTags()
    {
        $htmlContent = '<p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p><p class="p1">Maternity jeans 14.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank">New Look</a></p><p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p>; maternity jeans22.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-black-underbump-jeggings-_326137601" target="_blank"><p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p></a>';

        $expected = '<p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p><p class="p1">Maternity jeans 14.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-blue-underbump-skinny-jeans_361087840" target="_blank">New Look</a></p><p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p>; maternity jeans22.99 from <a href="http://www.newlook.com/eu/shop/maternity/jeans/maternity-black-underbump-jeggings-_326137601" target="_blank"><p><img class="alignnone size-full wp-image-238092" src="http://m0.herfamily.ie/wp-content/uploads/2016/03/01175014/jeans.png" alt="jeans" width="790" height="500"></p></a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyATags();


        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
