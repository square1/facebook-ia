<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class FigureWrappingTest extends TestCase
{

    /**
     * Test the remotion of wrapping p tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfPTagsWrappingFigureTags()
    {
        $htmlContent = '<p><figure data-mode="non-interactive"><img class="alignnone size-full wp-image-283636" src="http://m0.her.ie/wp-content/uploads/2016/03/19164750/dog-maternity-photo-shoot-lilica-ana-paula-grillo-21.jpg" alt="dog-maternity-photo-shoot-lilica-ana-paula-grillo-21" width="700" height="668"></figure>This p tag should stay as it is</p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();
        $expected = '<p><figure data-mode="non-interactive"><img class="alignnone size-full wp-image-283636" src="http://m0.her.ie/wp-content/uploads/2016/03/19164750/dog-maternity-photo-shoot-lilica-ana-paula-grillo-21.jpg" alt="dog-maternity-photo-shoot-lilica-ana-paula-grillo-21" width="700" height="668"></figure>This p tag should stay as it is</p>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h1 tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH1TagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>'.
            '<p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>'.
            '<h1><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" '.
            'height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></h1>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>'.
            '<p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>'.
            '<figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" '.
            'height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h2 tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH2TagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h2><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h1 tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH3TagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h3><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></h3>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h1 tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH4TagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h4><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></h4>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h5 tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH5TagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h5><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></h5>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h6 tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH6TagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h6><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></h6>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping a tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfATagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <a class="empty" href="#"><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping p and a tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfPAndATagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></p>
            <a class="empty" href="#"><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();
        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping strong and span tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfStrongAndSpanTagsWrappingFigureTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <strong><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></strong>
            <span class="empty" href="#"><figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure></span>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>
            <figure><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping p tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfPTagsWrappingFigureTagsWhenWrapTagHaveTextOnIt()
    {
        $htmlContent = '<p><figure data-mode="non-interactive"><img class="alignnone size-full wp-image-283636" src="http://m0.her.ie/wp-content/uploads/2016/03/19164750/dog-maternity-photo-shoot-lilica-ana-paula-grillo-21.jpg" alt="dog-maternity-photo-shoot-lilica-ana-paula-grillo-21" width="700" height="668"></figure>This p tag should stay as it is</p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();

        $expected = '<p><figure data-mode="non-interactive"><img class="alignnone size-full wp-image-283636" src="http://m0.her.ie/wp-content/uploads/2016/03/19164750/dog-maternity-photo-shoot-lilica-ana-paula-grillo-21.jpg" alt="dog-maternity-photo-shoot-lilica-ana-paula-grillo-21" width="700" height="668"></figure>This p tag should stay as it is</p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping nested tags around figure tags for instant articles
     *
     * @return void
     */
    public function testRemoveNestedWrappingTags()
    {
        $htmlContent = '<h2><b><figure data-mode="non-interactive"><img class="alignnone size-full wp-image-283636" src="http://m0.her.ie/wp-content/uploads/2016/03/19164750/dog-maternity-photo-shoot-lilica-ana-paula-grillo-21.jpg" alt="dog-maternity-photo-shoot-lilica-ana-paula-grillo-21" width="700" height="668"></figure></b></h2>';
        for ($i = 0; $i < 2; $i++) {
            $htmlContent = $this->getFormatterInstance($htmlContent)->removeFigureWrappingTags();
        }
        $expected = '<figure data-mode="non-interactive"><img class="alignnone size-full wp-image-283636" src="http://m0.her.ie/wp-content/uploads/2016/03/19164750/dog-maternity-photo-shoot-lilica-ana-paula-grillo-21.jpg" alt="dog-maternity-photo-shoot-lilica-ana-paula-grillo-21" width="700" height="668"></figure>';
        $this->assertEquals($expected, $htmlContent);
    }
}
