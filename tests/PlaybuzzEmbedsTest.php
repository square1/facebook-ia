<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class PlaybuzzEmbedsTest extends TestCase
{

    /**
     * Test PlayBuzz post format embeds function for InstantArticles
     * with multiple PlayBuzz embeds
     *
     * @return void
     */
    public function testPlayBuzzInstantArticleWithMultipleEmbeds()
    {
        $htmlContent = '<p><script src="//cdn.playbuzz.com/widget/feed.js"'.
        ' type="text/javascript"></script></p>'.
        '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
        'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
        'data-recommend="false" data-game-info="false" data-comments="false"></div>'.
        '<p><script src="//cdn.playbuzz.com/widget/feed.js"'.
        ' type="text/javascript"></script></p>'.
        '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
        'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
        'data-recommend="false" data-game-info="false" data-comments="false"></div>';

        $expected = '<figure class="op-social"><iframe>'.
        '<script src="//cdn.playbuzz.com/widget/feed.js"'.
        ' type="text/javascript"></script>'.
        '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
        'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
        'data-recommend="false" data-game-info="false" data-comments="false"></div>'.
        '</iframe></figure>'.
        '<figure class="op-social"><iframe>'.
        '<script src="//cdn.playbuzz.com/widget/feed.js"'.
        ' type="text/javascript"></script>'.
        '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
        'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
        'data-recommend="false" data-game-info="false" data-comments="false"></div>'.
        '</iframe></figure>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatPlayBuzz();
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test PlayBuzz post format embeds function for InstantArticles
     *
     * @return void
     */
    public function testPlayBuzzInstantArticle()
    {
        $htmlContent = '<p><script src="//cdn.playbuzz.com/widget/feed.js"'.
        ' type="text/javascript"></script></p>'.
        '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
        'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
        'data-recommend="false" data-game-info="false" data-comments="false"></div>';

        $expected = '<figure class="op-social"><iframe>'.
        '<script src="//cdn.playbuzz.com/widget/feed.js"'.
        ' type="text/javascript"></script>'.
        '<div class="pb_feed" data-embed-by="d8d6d6c8-312b-441a-aaee-b6ba4dec7bb4" '.
        'data-game="/jordang25/can-you-guess-all-these-difficult-dog-breeds" '.
        'data-recommend="false" data-game-info="false" data-comments="false"></div>'.
        '</iframe></figure>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatPlayBuzz();
        $this->assertEquals($expected, $htmlContent);
    }
}
