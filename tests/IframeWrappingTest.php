<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class IframeWrappingTest extends TestCase
{

    /**
     * Test the remotion of wrapping p tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfPTagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h1 tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH1TagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h1><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></h1>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h2 tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH2TagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h2><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></h2>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h3 tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH3TagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h3><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></h3>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h4 tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH4TagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h4><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></h4>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h5 tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH5TagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h5><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></h5>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping h6 tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfH6TagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <h6><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></h6>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping a tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfATagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <a class="empty" href="#"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping p and a tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfPAndATagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <p><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
            <a class="empty" href="#"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></a>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the remotion of wrapping strong and span tags around iframe tags for instant articles
     *
     * @return void
     */
    public function testRemotionOfStrongAndSpanTagsWrappingIframeTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <strong><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></strong>
            <span class="empty" href="#"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></span>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test iframe tags with no wrapping p or a tags
     *
     * @return void
     */
    public function testIframeTagsWithNoWrappingTags()
    {
        $htmlContent = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeIframeWrappingTags();

        $expected = '<p>Snow artist Simon Beck has created a massive tribute</p>
            <p>Walking all day and all night, through 32.5 kilometres of heavy Alpine snow</p>
            <iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
