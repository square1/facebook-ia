<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class EmptyHtmlTagsTest extends TestCase
{
    /**
     * Test the removal of all empty tags without spaces in post content
     *
     * @return void
     */
    public function testRemoveAllEmptyTagsFromContent()
    {

        $htmlContent = '<p></p><a href="#" class="empty"></a><span></span><figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure><script></script>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyHtmlTags();

        $expected = '<figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the removal of all empty tags but ignoring empty
     * facebook video empty <div id="fb-root"></div>
     *
     * @return void
     */
    public function testRemoveAllEmptyTagsFromContentWithFacebookEmptyDiv()
    {

        $htmlContent = '<p><a > </a>(Obviously you need to watch this on your phone to embrace the full'
            .'effects, but if youre on a computer you can drag the video around like some sort of cave-man&nbsp;'
            .'idiot and use your imagination. Also, if it doesnt work, try using Facebook app.)</p><div id="fb-root"></div>'
            .'<p><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; '
            .'js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; fj'
            .'s.parentNode.insertBefore(js, fjs);}(document, script, facebook-jssdk));</script></p><div class="fb-post" '
            .'data-href="https://www.facebook.com/jhatphoto/videos/1126525424058339/" data-width="500"><div class="fb-xfbml-pa'
            .'rse-ignore">';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyHtmlTags();

        $expected = '<p>(Obviously you need to watch this on your phone to embrace the full'
            .'effects, but if youre on a computer you can drag the video around like some sort of cave-man&nbsp;'
            .'idiot and use your imagination. Also, if it doesnt work, try using Facebook app.)</p><div id="fb-root"></div>'
            .'<p><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; '
            .'js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3"; fj'
            .'s.parentNode.insertBefore(js, fjs);}(document, script, facebook-jssdk));</script></p><div class="fb-post" '
            .'data-href="https://www.facebook.com/jhatphoto/videos/1126525424058339/" data-width="500"><div class="fb-xfbml-pa'
            .'rse-ignore"></div></div>';
        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the removal of all empty tags recursively
     *
     * @return void
     */
    public function testRemoveAllEmptyTagsRecursively()
    {

        $htmlContent = '<p></p><a href="#" class="empty"><strong><span></span></strong>  </a><span></span><figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure><script class="some"></script><script></script><p><p><p><p></p></p></p><div>not empty</div></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyHtmlTags();

        $expected = '<figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure><script class="some"></script><div>not empty</div>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the removal of all empty tags with spaces in post content
     *
     * @return void
     */
    public function testRemoveAllEmptyTagsWithSpacesFromContent()
    {

        $htmlContent = '<p> </p><a href="#" class="empty">  </a><span>   </span><figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure><script></script>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyHtmlTags();

        $expected = '<figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test that ignored tags are not being removed
     *
     * @return void
     */
    public function testIgnoredTagsNotRemovedFromContent()
    {

        $htmlContent = '<figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure><script></script>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->removeEmptyHtmlTags();

        $expected = '<figure class="op-interactive"><iframe class="giphy-embed" src="http://giphy.com/embed/26tPltHcocfDwk1YQ" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
