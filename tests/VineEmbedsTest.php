<?php

namespace Tests;

/**
* Facebook Instant Article content formatter test class
*/

class VineEmbedsTest extends TestCase
{

    /**
     * Test the vine embeds are formatted correctly for InstantArticles
     *
     * @return void
     */
    public function testFormatVine()
    {
        $htmlContent = '<h3>He was so close to bursting out laughing.</h3>
            <p>Everton fans might want to turn their TV off this evening after Martinez men suffered .</p>
            <p>Gareth Barry hasarguably been Everton most consistent performer this season and the former
            Aston Villa/Man City man has been an incredibly shrewd acquisition since he joined the club on a
            permanent deal in 2014.</p><p>The holdingne of the best players England have had.</p>
            <p>Henry must be a Scholes, Gerrard or Heskey type of man.</p>
            <p><iframe src="https://vine.co/v/iiAWvg7tWbI/embed/simple" width="600" height="600" frameborder="0"></iframe>
            <script src="https://platform.vine.co/static/scripts/embed.js"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatVine();

        $expected = '<h3>He was so close to bursting out laughing.</h3>
            <p>Everton fans might want to turn their TV off this evening after Martinez men suffered .</p>
            <p>Gareth Barry hasarguably been Everton most consistent performer this season and the former
            Aston Villa/Man City man has been an incredibly shrewd acquisition since he joined the club on a
            permanent deal in 2014.</p><p>The holdingne of the best players England have had.</p>
            <p>Henry must be a Scholes, Gerrard or Heskey type of man.</p>
            <figure class="op-social"><iframe src="https://vine.co/v/iiAWvg7tWbI/embed/simple" width="600" height="600" frameborder="0"></iframe>
            <script src="https://platform.vine.co/static/scripts/embed.js"></script></figure>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }

    /**
     * Test the vine format embeds function for InstantArticles
     * without or with similar vine info.
     *
     * @return void
     */
    public function testFormatVineForInstantArticleNoneVineContent()
    {
        $htmlContent = '<p>Enjoy/wince...</p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><p>
            <script async src="//platform.vine.co/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a></p>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        $htmlContent = $this->getFormatterInstance($htmlContent)->formatVine();

        $expected = '<p>Enjoy/wince...</p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">I blue myself.
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a> <a href="https://twitter.com/midnight">@midnight</a>
            <a href="https://t.co/EJQZL9FDXd">pic.twitter.com/EJQZL9FDXd</a></p><p>&mdash; Arrested Development (@bluthquotes)
            <a href="https://twitter.com/bluthquotes/status/707068187754438656">March 8, 2016</a></p></blockquote><p>
            <script async src="//platform.vine.co/widgets.js" charset="utf-8"></script></p>
            <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> <a href="https://t.co/qc0WiqYYvM">pic.twitter.com/qc0WiqYYvM</a>
            <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Excellent boiled potatoes <a href="https://twitter.com/hashtag/BadSexIn3Words?src=hash">#BadSexIn3Words</a>
            <a href="https://twitter.com/midnight">@midnight</a> -b <a href="https://t.co/AUXfDGDEYh">pic.twitter.com/AUXfDGDEYh</a></p><p>&mdash; Drunk Austen (@Drunk_Austen)
            <a href="https://twitter.com/Drunk_Austen/status/707102922450931712">March 8, 2016</a></p></blockquote><p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>';

        // Check we have a right formatted content
        $this->assertEquals($expected, $htmlContent);
    }
}
