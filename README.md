# README #

### Facebook instant article posts formatter ###

### How do I get set up? ###

* Add the custom repository to composer.json

```
#!php
"repositories":[
        {
            "type": "vcs",
            "url": "https://bitbucket.org/square1/facebook-ia-formatter"
        }
    ],

```

* Add the package to the "require" list


```
#!php
"square1/facebook-ia": "dev-master"

```

* Composer update


```
#!php

$ composer update
```

* Add the service provider to config.php


```
#!php

Square1\Formatter\FormatterServiceProvider::class
```

* Add the follow var to the .env file (using your website url)

```
#!php

SITE_URL=http://www.example.com
```

### How to use? ###

```
#!php

$formatter = new Square1\Formatter\Formatter(
            $htmlContent,
            $postUrl
        );
$facebookIAContent = $formatter->{functionName}();
```

### Available functions ###


```
#!php
        // This one involves all the functions below
        $formatter->getFormattedContent()

        $formatter->setWellFormattedHTMLContent();
        $formatter->formatYouTube();
        $formatter->formatTwitter();
        $formatter->formatVine();
        $formatter->formatInstagram();
        $formatter->formatFacebookVideo();
        $formatter->formatFacebookPost();
        $formatter->formatVimeo();
        $formatter->formatImgur();
        $formatter->formatStreamable();
        $formatter->formatRssPost();
        $formatter->removeUnsupportedHeadingTypes();
        $formatter->formatHeadings();
        $formatter->removeHtmlAroundImages();
        $formatter->formatPollDaddy();
        $formatter->formatPlayBuzz();
        $formatter->removeEmptyATags();
        $formatter->removeEmptyHtmlTags();
        $formatter->wrapImgWithFigureTag();
        $formatter->completeRelativeUrls();
        $formatter->removeIframeWrappingTags();
        $formatter->removeFigureWrappingTags();
        $formatter->removeEmptyPTags();
        $formatter->formatOoyala();
        $formatter->formatSoundcloud();
        $formatter->formatContentBlockquotes();
        $formatter->formatContentAnchords();
        $formatter->removeWidthAndHeightFromEmbeds();
        $formatter->removeWidthAndHeightFromIframes();
```